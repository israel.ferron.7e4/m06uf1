package UF2.Practica1.Classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {
    private static Connection connection;

    public ConnectionDB(Connection connection) {
        this.connection = connection;
    }

    public static Connection conect(){
        try {
            connection = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/empresa", "empresa", "empresa");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void close(Connection con){
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
