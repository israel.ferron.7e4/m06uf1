package UF2.Practica1.Metodes;


import UF2.Practica1.Classes.ConnectionDB;

import java.io.*;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class MetodesPr1 {
    Scanner scanner;

    //Contructor
    public MetodesPr1(Scanner scanner) {
        this.scanner = scanner;

    }

    public MetodesPr1() {
    }

    public void executeScript() {
        File file = new File("empresa.sql");
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            System.out.println("ERROR NO HAY FILE: " + e.getMessage());
        }
        String linea = null;
        StringBuilder stringBuilder = new StringBuilder();
        String salto = System.getProperty("line.separator");
        try {
            while ((linea = entrada.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }
        } catch (IOException e) {
            System.out.println("ERROR de E/S, al operar " + e.getMessage());
        }
        String consulta = stringBuilder.toString();
        System.out.println(consulta);
        try {
            Connection connection = ConnectionDB.conect();
            Statement sents = connection.createStatement();
            int res = sents.executeUpdate(consulta);
            System.out.println("Script creado con éxito, res = " + res);
            ConnectionDB.close(connection);
            sents.close();
        } catch (SQLException e) {
            System.out.println("ERROR AL EJECUTAR EL SCRIPT: " + e.getMessage());
        }
    }

    public void Ex1() {
        try {
            Connection connection = ConnectionDB.conect();
            int[] nums = {300388, 400552, 400333};
            String[] desc = {"RH GUIDE TO PADDLE", "RH GUIDE TO BOX", "ACE TENNIS BALLS-10 PACK"};
            String query = "INSERT INTO producte VALUES "
                    + "( ?, ? )";
            for (int i = 0; i < nums.length; i++) {
                PreparedStatement ps =
                        connection.prepareStatement(query);
                ps.setInt(1, nums[i]);
                ps.setString(2, desc[i]);
                ps.executeUpdate();
                ps.close();
            }
            ConnectionDB.close(connection);
            System.out.println("DADES INSERTADES CORRECTAMENT");
        } catch (SQLException e) {
        }
    }

    public void Ex2() {
        try {
            Connection connection = ConnectionDB.conect();
            int[] nums = {4885, 8772, 9945};
            String[] cognoms = {"BORREL", "PUIG", "FERRER"};
            String[] ofici = {"EMPLEAT", "VENDEDOR", "ANALISTA"};
            int[] cap = {7902, 7698, 7698};
            String[] data = {"1981-12-25", "1990-01-23", "1988-05-17"};
            int[] salari = {104000, 108000, 169000};
            int[] comm = {0, 0, 39000};
            int[] dep = {30, 30, 20};
            String query = "INSERT INTO emp VALUES "
                    + "( ?, ?, ?, ?, ?, ?, ?, ? )";
            for (int i = 0; i < nums.length; i++) {
                PreparedStatement ps =
                        connection.prepareStatement(query);
                ps.setInt(1, nums[i]);
                ps.setString(2, cognoms[i]);
                ps.setString(3, ofici[i]);
                ps.setInt(4, cap[i]);
                ps.setDate(5, java.sql.Date.valueOf(data[i]));
                ps.setInt(6, salari[i]);
                if (comm[i] == 0) {
                    ps.setObject(7, null);
                } else {
                    ps.setInt(7, comm[i]);
                }
                ps.setInt(8, dep[i]);
                ps.executeUpdate();
                ps.close();
            }
            ConnectionDB.close(connection);
            System.out.println("DADES INSERTADES CORRECTAMENT");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void Ex3() {
        try {
            Connection connection = ConnectionDB.conect();
            int[] cod = {104, 106, 107};
            int[] limit = {20000, 12000, 20000};

            String query = "UPDATE client SET limit_credit = ? WHERE client_cod = ?";
            for (int i = 0; i < cod.length; i++) {
                PreparedStatement ps =
                        connection.prepareStatement(query);
                ps.setInt(1, limit[i]);
                ps.setInt(2, cod[i]);
                ps.executeUpdate();
                ps.close();
            }
            ConnectionDB.close(connection);
            System.out.println("DADES ACTUALITZADES CORRECTAMENT");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex4() {
        try {
            Connection connection = ConnectionDB.conect();
            Statement query = connection.createStatement();
            String sel = "SELECT * FROM client where client_cod = 106";
            boolean valor = query.execute(sel);
            if (valor) {
                ResultSet rs = query.getResultSet();
                int n = 1;
                while (rs.next()) {
                    System.out.printf("Cod: %d \n" +
                            "NOM: %s \n" +
                            "ADRESS: %s \n" +
                            "CIUTAT: %s \n" +
                            "ESTAT: %s \n" +
                            "CP: %s \n" +
                            "AREA: %d \n" +
                            "TEL.: %s \n" +
                            "JEFE: %d \n" +
                            "LIMIT: %d \n" +
                            "OBS: %s \n", rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getString(8), rs.getInt(9), rs.getInt(10), rs.getString(11));
                }
            } else {
                int f = query.getUpdateCount();
                System.out.printf("Filas afectadas:%d \n", f);
            }

            query.close();
            connection.close();
        } catch (SQLException e) {
        }
    }

    public void Ex5() {
        try {
            Connection connection = ConnectionDB.conect();
            Statement query = connection.createStatement();
            String sel = "SELECT * FROM emp where emp_no = 7788";
            boolean valor = query.execute(sel);
            if (valor) {
                ResultSet rs = query.getResultSet();
                int n = 1;
                while (rs.next()) {
                    System.out.printf("Nº: %d \n" +
                            "COGNOM: %s \n" +
                            "OFICI: %s \n" +
                            "CAP: %d \n" +
                            "DATA_ALTA: %s \n" +
                            "SALARI: %d \n" +
                            "COMM.: %d \n" +
                            "DEP: %d \n", rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getDate(5).toString(), rs.getInt(6), rs.getInt(7), rs.getInt(8));

                }
            } else {
                int f = query.getUpdateCount();
                System.out.printf("Filas afectadas:%d \n", f);
            }

            query.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void Ex6() {
        try {
            Connection connection = ConnectionDB.conect();
            String query = "Select * FROM producte Where prod_num = ?";
            PreparedStatement ps =
                    connection.prepareStatement(query);
            ps.setInt(1, 101860);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                System.out.printf("COD: %d \n" +
                        "DESC.: %s \n", result.getInt(1), result.getString(2));
            }
            ps.close();
            ConnectionDB.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex7() {
        try {
            Connection connection = ConnectionDB.conect();

            String query = "DELETE FROM client WHERE client_cod = ?";
            PreparedStatement ps =
                    connection.prepareStatement(query);
            ps.setInt(1, 109);
            ps.executeUpdate();
            ps.close();
            ConnectionDB.close(connection);
            System.out.println("DADES ELIMINADES CORRECTAMENT");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex8(){
        try {
            Connection connection = ConnectionDB.conect();

            String query = "delete from emp where emp_no = 4885";
            Statement sentencia = connection.createStatement();
            try {
                sentencia.execute(query.toString());

            } catch (SQLException e) {
                //e.printStackTrace();
                System.out.printf("HA OCURRIDO UNA EXCEPCI�N:%n");
                System.out.printf("Mensaje   : %s %n", e.getMessage());
                System.out.printf("SQL estado: %s %n", e.getSQLState());
                System.out.printf("C�d error : %s %n", e.getErrorCode());
            }

            System.out.println("Dades Eliminades");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex9(){
        try {
            Connection connection = ConnectionDB.conect();

            String query = "DELETE FROM producte WHERE prod_num = ?";
            PreparedStatement ps =
                    connection.prepareStatement(query);
            ps.setInt(1, 400552);
            ps.executeUpdate();
            ps.close();
            ConnectionDB.close(connection);
            System.out.println("DADES ELIMINADES CORRECTAMENT");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
