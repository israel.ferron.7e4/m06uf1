package UF2.Practica1.Menu;



import UF2.Practica1.Metodes.MetodesPr1;

import java.io.IOException;
import java.util.Scanner;

public class MenuPr1 extends Menu {
    MetodesPr1 metodesPr1;

    public MenuPr1(Scanner scanner){
        super(scanner);
        metodesPr1 = new MetodesPr1(scanner);
        addOption("Practica 1");
        addOption("Inserta nous productes");
        addOption("Insertar nous empleats");
        addOption("Actualitzar el limit de credit dels clientes");
        addOption("Consultar dades del cliente 106");
        addOption("Consultar dades del empleat 7788");
        addOption("Consulta les dades del producte 101860");
        addOption("Eliminar el client amb codi 109");
        addOption("Eliminar l’empleat amb codi 4885.");
        addOption("Eliminar el producte 400552");

    }

    @Override
    public void action(int select) throws IOException, ClassNotFoundException {
        switch (select){
            case 1:
                metodesPr1.Ex1();
                break;
            case 2:
                metodesPr1.Ex2();
                break;
            case 3:
                metodesPr1.Ex3();
                break;
            case 4:
                metodesPr1.Ex4();
                break;
            case 5:
                metodesPr1.Ex5();
                break;
            case 6:
                metodesPr1.Ex6();
                break;
            case 7:
                metodesPr1.Ex7();
                break;
            case 8:
                metodesPr1.Ex8();
                break;
            case 9:
                metodesPr1.Ex9();
                break;
        }
    }

}