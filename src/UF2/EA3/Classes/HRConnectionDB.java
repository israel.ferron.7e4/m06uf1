package UF2.EA3.Classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HRConnectionDB {
    private static Connection connection;

    public HRConnectionDB(Connection connection) {
        this.connection = connection;
    }

    public static Connection conect(){
        try {
            connection = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/hr", "hr", "hr");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void close(Connection con){
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
