package UF2.EA3.Metodes;


import UF2.EA3.Classes.HRConnectionDB;
import UF2.EA3.Menu.Color;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MetodesEA3 {
    Scanner scanner;

    //Contructor
    public MetodesEA3(Scanner scanner) {
        this.scanner = scanner;
    }


    public void executeScript() {
        File file = new File("HR.sql");
        System.out.println("\n\nFichero de consulta : " + file.getName());
        System.out.println("Convirtiendo el fichero a cadena...");
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            System.out.println("ERROR NO HAY FILE: " + e.getMessage());
        }
        String linea = null;
        StringBuilder stringBuilder = new StringBuilder();
        String salto = System.getProperty("line.separator");
        try {
            while ((linea = entrada.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }
        } catch (IOException e) {
            System.out.println("ERROR de E/S, al operar " + e.getMessage());
        }
        String consulta = stringBuilder.toString();
        System.out.println(consulta);
        try {
            Connection connmysql = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/hr", "hr", "hr");
            Statement sents = connmysql.createStatement();
            int res = sents.executeUpdate(consulta);
            System.out.println("Script creado con éxito, res = " + res);
            connmysql.close();
            sents.close();
        } catch (SQLException e) {
            System.out.println("ERROR AL EJECUTAR EL SCRIPT: " + e.getMessage());
        }
    }

    public void Ex1() {
        try {
            Connection connection = HRConnectionDB.conect();
            DatabaseMetaData dbmd = connection.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            String nombre = dbmd.getDatabaseProductName();
            String driver = dbmd.getDriverName();
            String url = dbmd.getURL();
            String usuario = dbmd.getUserName();

            System.out.println("INFORMACI�N SOBRE LA BASE DE DATOS:");
            System.out.println("===================================");
            System.out.printf("Nombre : %s %n", nombre);
            System.out.printf("Driver : %s %n", driver);
            System.out.printf("URL    : %s %n", url);
            System.out.printf("Usuario: %s %n", usuario);

            //Obtener informaci�n de las tablas y vistas que hay
            resul = dbmd.getTables(null, null, "empleados", null);

            while (resul.next()) {
                String catalogo = resul.getString(1);//columna 1
                String esquema = resul.getString(2); //columna 2
                String tabla = resul.getString(3);   //columna 3
                String tipo = resul.getString(4);      //columna 4
                System.out.printf("%s - Catalogo: %s, Esquema: %s, Nombre: %s %n",
                        tipo, catalogo, esquema, tabla);
            }

            resul = dbmd.getTables(null, null, "departamentos", null);

            while (resul.next()) {
                String catalogo = resul.getString(1);//columna 1
                String esquema = resul.getString(2); //columna 2
                String tabla = resul.getString(3);   //columna 3
                String tipo = resul.getString(4);      //columna 4
                System.out.printf("%s - Catalogo: %s, Esquema: %s, Nombre: %s %n",
                        tipo, catalogo, esquema, tabla);
            }
            HRConnectionDB.close(connection); //Cerrar conexion
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }//fin de main

    public void Ex2() {
        try {
            Connection connection = HRConnectionDB.conect();

            DatabaseMetaData dbmd = connection.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("COLUMNAS TABLA EMPLEADOS:");
            System.out.println("===================================");
            ResultSet columnas = null;
            columnas = dbmd.getColumns(null, null, "empleados", null);
            while (columnas.next()) {
                String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                String nula = columnas.getString("IS_NULLABLE");   //getString(18)

                System.out.printf("Columna: %s, Tipo: %s, Tama�o: %s, �Puede ser Nula:? %s %n", nombCol, tipoCol, tamCol, nula);
            }


            HRConnectionDB.close(connection); //Cerrar conexion
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }//fin de main

    public void Ex3() {
        try {
            Connection connection = HRConnectionDB.conect();

            DatabaseMetaData dbmd = connection.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("COLUMNAS TABLA DEPARTAMENTOS:");
            System.out.println("===================================");
            ResultSet columnas = null;
            columnas = dbmd.getColumns(null, null, "departamentos", null);
            while (columnas.next()) {
                String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                String nula = columnas.getString("IS_NULLABLE");   //getString(18)

                System.out.printf("Columna: %s, Tipo: %s, Tama�o: %s, �Puede ser Nula:? %s %n", nombCol, tipoCol, tamCol, nula);
            }


            HRConnectionDB.close(connection); //Cerrar conexion
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }//fin de main

    public void Ex4() {
        try {
            Connection connection = HRConnectionDB.conect();

            DatabaseMetaData dbmd = connection.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("CLAVES TABLA DEPARTAMENTOS:");
            System.out.println("===================================");
            ResultSet pk = dbmd.getPrimaryKeys(null, null, "departamentos");
            String pkDep = "", separador = "";
            while (pk.next()) {
                pkDep = pkDep + separador +
                        pk.getString("COLUMN_NAME");//getString(4)
                separador = "+";
            }

            System.out.println("Clave Primaria: " + pkDep);
            ResultSet fk = dbmd.getExportedKeys(null, null, "departamentos");

            while (fk.next()) {
                String fk_name = fk.getString("FKCOLUMN_NAME");
                String pk_name = fk.getString("PKCOLUMN_NAME");
                String pk_tablename = fk.getString("PKTABLE_NAME");
                String fk_tablename = fk.getString("FKTABLE_NAME");
                System.out.printf("Tabla PK: %s, Clave Primaria: %s %n", pk_tablename, pk_name);
                System.out.printf("Tabla FK: %s, Clave Ajena: %s %n", fk_tablename, fk_name);
            }

            System.out.println("CLAVES TABLA EMPLEADOS:");
            System.out.println("===================================");
            ResultSet pk2 = dbmd.getPrimaryKeys(null, null, "empleados");
            String pkEmp = "", separador2 = "";
            while (pk2.next()) {
                pkEmp = pkEmp + separador2 +
                        pk2.getString("COLUMN_NAME");//getString(4)
                separador2 = "+";
            }
            System.out.println("Clave Primaria: " + pkEmp);
            ResultSet fk2 = dbmd.getExportedKeys(null, null, "empleados");

            while (fk2.next()) {
                String fk_name = fk2.getString("FKCOLUMN_NAME");
                String pk_name = fk2.getString("PKCOLUMN_NAME");
                String pk_tablename = fk2.getString("PKTABLE_NAME");
                String fk_tablename = fk2.getString("FKTABLE_NAME");
                System.out.printf("Tabla PK: %s, Clave Primaria: %s %n", pk_tablename, pk_name);
                System.out.printf("Tabla FK: %s, Clave Ajena: %s %n", fk_tablename, fk_name);
            }


            connection.close(); //Cerrar conexion
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }//fin de main

    public void Ex5() {
        try {
            Connection connection = HRConnectionDB.conect();
            Statement query = connection.createStatement();
            String selNota = "SELECT * from empleados";
            boolean valor = query.execute(selNota);

            if (valor) {
                ResultSet rs = query.getResultSet();
                int n = 1;
                while (rs.next()) {
                    System.out.printf("ID: %d \n" +
                                    "Apellido: %s \n" +
                                    "Oficio: %s \n" +
                                    "Dir: %d\n" +
                                    "Fecha Alta: %s \n" +
                                    "Salario: %.1f \n" +
                                    "Comission: %.1f \n" +
                                    "Id Depr: %d \n" +
                                    "------------------- \n",
                            rs.getInt(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getInt(4),
                            rs.getDate(5).toString(),
                            rs.getDouble(6),
                            rs.getDouble(7),
                            rs.getInt(8));
                    n++;
                }
            } else {
                int f = query.getUpdateCount();
                System.out.printf("Filas afectadas:%d \n", f);
            }
            connection.close();
        } catch (SQLException ex) {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
        }

    }

    public void Ex6() {
        try {
            Connection connection = HRConnectionDB.conect();
            Statement query = connection.createStatement();
            String selNota = "SELECT * from departamentos";
            boolean valor = query.execute(selNota);

            if (valor) {
                ResultSet rs = query.getResultSet();
                int n = 1;
                while (rs.next()) {
                    System.out.printf("ID: %d \nNombre:%s \nLocation:%s \nNota:%s\n -----------------\n", n,
                            rs.getString(1),
                            rs.getString(2),
                            rs.getString(3));
                    n++;
                }
            } else {
                int f = query.getUpdateCount();
                System.out.printf("Filas afectadas:%d \n", f);
            }
            connection.close();
        } catch (SQLException ex) {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
        }
    }

    public void Ex7() {
        try {
            Connection connection = HRConnectionDB.conect();
            String query = "INSERT INTO departamentos VALUES "
                    + "( ?, ?, ?)";
            for (int i = 1; i < 4; i++) {
                System.out.println(Color.CYAN + Color.BOLD + "Insertant Departanemt Nº " + i);
                System.out.println("Id del departament:");
                int id = scanner.nextInt();
                System.out.println("Nom del departament:");
                scanner.nextLine();
                String nom = scanner.nextLine();
                System.out.println("Localitat del departament:");
                String loc = scanner.nextLine();
                System.out.println("Dades a insertar -> " +
                        "Id: " + id + "\n" +
                        "Nom: " + nom + "\n" +
                        "Localitat: " + loc);
                PreparedStatement ps =
                        connection.prepareStatement(query);
                ps.setInt(1, id);
                ps.setString(2, nom);
                ps.setString(3, loc);
                ps.executeUpdate();
                ps.close();
                System.out.println(Color.ITALIC + Color.PURPLE + "Dades Insertades Correctament\n" + Color.RESET);
            }
            HRConnectionDB.close(connection);
        } catch (SQLException ex) {
            System.out.println(ex.getErrorCode());
        }
    }

    public void Ex8() {
        Connection connection = HRConnectionDB.conect();
        String query = "INSERT INTO empleados VALUES "
                + "( ?, ?, ?, ?, ?, ?, ?, ?)";
        try {

            for (int i = 1; i < 4; i++) {
                System.out.println(Color.CYAN + Color.BOLD + "Insertant Empleat Nº " + i);
                System.out.println("Id del empleat:");
                int id = scanner.nextInt();
                System.out.println("Cognom del empleat:");
                scanner.nextLine();
                String cognom = scanner.nextLine();
                System.out.println("Ofici del empleat:");
                String ofici = scanner.nextLine();
                System.out.println("Director del empleat:");
                int dir = scanner.nextInt();
                System.out.println("Data de alta del empleat: (Formato 0000-00-00)");
                scanner.nextLine();
                String data = scanner.nextLine();
                System.out.println("Salari del empleado:");
                double salario = scanner.nextDouble();
                System.out.println("Comissio del empleat:");
                double com = scanner.nextDouble();
                System.out.println("Numero de Departament de lempleat:");
                int dep = scanner.nextInt();
                System.out.println("Dades a insertar -> " +
                        "Id: " + id + "\n" +
                        "Cognom: " + cognom + "\n" +
                        "Ofici: " + ofici + "\n" +
                        "Director: " + dir + "\n" +
                        "Fecha de Alta: " + data + "\n" +
                        "Salari: " + salario + "\n" +
                        "Comissio: " + com + "\n" +
                        "Departament: " + dep);
                PreparedStatement ps =
                        connection.prepareStatement(query);
                ps.setInt(1, id);
                ps.setString(2, cognom.toUpperCase());
                ps.setString(3, ofici);
                ps.setInt(4, dir);
                ps.setDate(5, java.sql.Date.valueOf(data));
                ps.setDouble(6, salario);
                ps.setDouble(7, com);
                ps.setInt(8, dep);
                ps.execute();
                ps.close();
                System.out.println(Color.ITALIC + Color.PURPLE + "Dades Insertades Correctament\n" + Color.RESET);
            }
            HRConnectionDB.close(connection);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void Ex9() {
        try {
            double salario = 0;
            Connection connection = HRConnectionDB.conect();
            System.out.println(Color.CYAN + Color.BOLD + "Posa el numero de Empleat que vols modificar el salari:");
            int id = scanner.nextInt();
            System.out.println("Posa el salari que li vols sumar:");
            double salariAdd = scanner.nextDouble();
            String sel = "Select salario FROM empleados Where emp_no = ?";
            PreparedStatement psSel =
                    connection.prepareStatement(sel);
            psSel.setInt(1, id);
            psSel.execute();
            ResultSet rs = psSel.getResultSet();
            while (rs.next()) {
                salario = rs.getDouble(1);
            }
            psSel.close();
            salario += salariAdd;
            String update = "UPDATE empleados SET salario = ? WHERE emp_no = ?";
            PreparedStatement psUpdate =
                    connection.prepareStatement(update);
            psUpdate.setDouble(1, salario);
            psUpdate.setInt(2, id);
            psUpdate.execute();
            psUpdate.close();
            System.out.println(Color.ITALIC + Color.PURPLE + "Dades Cambiades Correctament\n" + Color.RESET);
        } catch (SQLException ex) {
        }
    }

    public void Ex10() {
        try {
            Connection connection = HRConnectionDB.conect();
            StringBuilder sql = new StringBuilder();
            sql.append("CREATE OR REPLACE VIEW totales ");
            sql.append("(dep, dnombre, nemp, media) AS ");
            sql.append("SELECT d.dept_no, dnombre, COUNT(emp_no), AVG(salario) ");
            sql.append("FROM departamentos d LEFT JOIN empleados e ");
            sql.append("ON e.dept_no = d.dept_no ");
            sql.append("GROUP BY d.dept_no, dnombre ");
            System.out.println(sql);

            Statement sentencia = connection.createStatement();
            int filas = sentencia.executeUpdate(sql.toString());
            System.out.printf("Resultado  de la ejecuci�n: %d %n", filas);

            sentencia.close(); // Cerrar Statement

            Statement query = connection.createStatement();
            String selNota = "SELECT * from totales";
            boolean valor = query.execute(selNota);

            if (valor) {
                ResultSet rs = query.getResultSet();
                while (rs.next()) {
                    System.out.printf("Dep: %d \n" +
                                    "Nombre: %s \n" +
                                    "Nº Emp: %d \n" +
                                    "Avg Salari: %.1f\n" +
                                    "------------------- \n",
                            rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getDouble(4));
                }
            }
            HRConnectionDB.close(connection);
        } catch (SQLException ex) {
        }
    }

    public void Ex11() {
        try {
            Connection connection = HRConnectionDB.conect();

            StringBuilder sql = new StringBuilder();
            sql.append("CREATE PROCEDURE subida_sal (d INT, subida INT) AS $$");
            sql.append("BEGIN ");
            sql.append("UPDATE empleados SET salario = salario + subida WHERE dept_no = d; ");
            sql.append("COMMIT; ");
            sql.append("END; ");
            sql.append("$$LANGUAGE plpgsql");

            System.out.println(sql);

            Statement sentencia = connection.createStatement();
            int filas = sentencia.executeUpdate(sql.toString());
            System.out.printf("Resultado  de la ejecuci�n: %d %n", filas);

            sentencia.close(); // Cerrar Statement
            HRConnectionDB.close(connection);
            System.out.println("Procediment Creat Correctament");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void Ex12() {
        try {
            Connection connection = HRConnectionDB.conect();

            String sql = " CREATE FUNCTION nombre_dep(dep INT) RETURNS VARCHAR(15) AS $$ DECLARE nom VARCHAR(15) DEFAULT 'INEXISTENT';BEGIN SELECT dnombre INTO nom FROM departamentos WHERE dept_no = dep; RETURN nom; END;$$LANGUAGE plpgsql;";

            Statement sentencia = connection.createStatement();
            int filas = sentencia.executeUpdate(sql);

            System.out.printf("Resultat de l'execuci�: %d %n", filas);

            System.out.println(sql);

            sentencia.close(); // Cerrar Statement
            HRConnectionDB.close(connection);
            System.out.println("Function Creat Correctament");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void Ex13() {
        try {

            Connection connection = HRConnectionDB.conect();
            DatabaseMetaData dbmd = connection.getMetaData();// Creamos objeto
            // DatabaseMetaData

            ResultSet proc = dbmd.getProcedures(null, null, null);
            while (proc.next()) {
                String proc_name = proc.getString("PROCEDURE_NAME");
                String proc_type = proc.getString("PROCEDURE_TYPE");
                System.out.printf("Nombre Procedimiento: %s - Tipo: %s %n", proc_name, proc_type);
            }
            ResultSet func = dbmd.getFunctions(null, null, "nombre_dep35");
            while (func.next()) {
                String func_name = func.getString("FUNCTION_NAME");
                String funt_type = func.getString("FUNCTION_TYPE");
                System.out.printf("Nombre Funcion: %s - Tipo: %s %n", func_name, funt_type);
            }

            HRConnectionDB.close(connection);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void Ex14() {
        try {
            Connection connection = HRConnectionDB.conect();
            // recuperar parametros de main
            int dep = 10;   //"10"; // departamento
            int subida = 1000;//"1000"; // subida

            // construir orden DE LLAMADA
            String sql = "call subida_sal (?, ?)";

            // Preparamos la llamada
            CallableStatement llamada = connection.prepareCall(sql);
            // Damos valor a los argumentos
            llamada.setInt(1, dep); // primer argumento-dep
            llamada.setInt(2, subida); // segundo arg

            llamada.execute(); // ejecutar el procedimiento
            System.out.println("Subida realizada....");
            llamada.close();
            HRConnectionDB.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex15(){
        try {

            Connection connection = HRConnectionDB.conect();

            // recuperar parametro de main
            int dep = 10;// args[0]; //departamento

            // MYSQL
            // CREATE FUNCTION nombre_dep(d int) RETURNS VARCHAR(15)
            // CREATE PROCEDURE datos_dep(d int, OUT nom VARCHAR(15),
            // OUT locali VARCHAR(15))

            String sql = "{? = call nombre_dep (?)}"; // MYSQL

            // Preparamos la llamada
            CallableStatement llamada = connection.prepareCall(sql);

            llamada.registerOutParameter(1, Types.VARCHAR); // valor devuelto
            llamada.setInt(2, dep); // param de entrada

            llamada.executeUpdate(); // ejecutar el procedimiento
            System.out.println("Nombre Dep: " + llamada.getString(1));
            llamada.close();
            HRConnectionDB.close(connection);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

