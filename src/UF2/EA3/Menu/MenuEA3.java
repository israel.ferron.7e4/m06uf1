package UF2.EA3.Menu;

import UF2.EA3.Metodes.MetodesEA3;

import java.io.IOException;
import java.util.Scanner;

public class MenuEA3 extends Menu {
    MetodesEA3 metodesEA3;

    public MenuEA3(Scanner scanner){
        super(scanner);
        metodesEA3 = new MetodesEA3(scanner);
        addOption("Exercicis EA3 UF2");
        addOption("Mostrar les metadades de la base de dades HR i les seves taules");
        addOption("Mostrar la informació dels camps o columnes de la taula empleados.");
        addOption("Mostrar la informació dels camps o columnes de la taula departamentos.");
        addOption("Mostrar les claus primàries i foranies de les taules empleados i departamentos");
        addOption("Mostrar totes les dades emmagatzemades en la taula empleados.");
        addOption("Mostrar totes les dades emmagatzemades en la taula departamentos.");
        addOption("Insertar 3 departaments a la taula \"departamentos\" utilitzant el PreparedStatement.");
        addOption("Insertar 3 departaments a la taula \"empleados\" utilitzant el PreparedStatement.");
        addOption("Actualitzar el salari d’un empleat. L'aplicació rep pel teclat l'identificador de l'empleat i la quantitat que\n" +
                "s'incrementa el salari.");
        addOption("Crear una vista, utilitza l’exemple de la classe \"CrearVista.java\" i fes una consulta (SELECT) sobre la\n" +
                "vista creada.");
        addOption("Crear un procediment amb el nom \"subida_sal\" utilitzat per incrementar el salari de tots els empleats d'un\n" +
                "departament,");
        addOption("Crear una funció amb el nom \"nombre_dep\"");
        addOption("Mostrar els procediments i funcions");
        addOption("Cridar (CALL) el procediment creat \"subida_sal\"");
        addOption("Cridar (CALL) la funció \"nombre_dep\"");
    }

    @Override
    public void action(int select) throws IOException {
        switch (select) {
            case 1:
                metodesEA3.Ex1();
                break;
            case 2:
                metodesEA3.Ex2();
                break;
            case 3:
                metodesEA3.Ex3();
                break;
            case 4:
                metodesEA3.Ex4();
                break;
            case 5:
                metodesEA3.Ex5();
                break;
            case 6:
                metodesEA3.Ex6();
                break;
            case 7:
                metodesEA3.Ex7();
                break;
            case 8:
                metodesEA3.Ex8();
                break;
            case 9:
                metodesEA3.Ex9();
                break;
            case 10:
                metodesEA3.Ex10();
                break;
            case 11:
                metodesEA3.Ex11();
                break;
            case 12:
                metodesEA3.Ex12();
                break;
            case 13:
                metodesEA3.Ex13();
                break;
            case 14:
                metodesEA3.Ex14();
                break;
            case 15:
                metodesEA3.Ex15();
                break;
        }
    }

}
