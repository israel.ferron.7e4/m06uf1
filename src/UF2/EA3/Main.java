package UF2.EA3;

import UF2.EA3.Menu.Color;
import UF2.EA3.Menu.MenuEA3;
import UF2.EA3.Metodes.MetodesEA3;
import UF2.Practica1.Metodes.MetodesPr1;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MetodesEA3 metodesEA3 = new MetodesEA3(scanner);
        metodesEA3.executeScript();
        MenuEA3 menuEA1 = new MenuEA3(scanner);
        menuEA1.showMenu();


        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!" + Color.RESET);

    }
}
