package UF2.EA4.Clases;

import java.util.Date;
import java.util.Objects;

public class EmpleadosEntity {
    private int empNo;
    private String apellido;
    private String oficio;
    private Integer dir;
    private Date fechaAlt;
    private Double salario;
    private Double comision;
    private DepartamentosEntity departamentosByDeptNo;

    public int getEmpNo() {
        return empNo;
    }

    public void setEmpNo(int empNo) {
        this.empNo = empNo;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getOficio() {
        return oficio;
    }

    public void setOficio(String oficio) {
        this.oficio = oficio;
    }

    public Integer getDir() {
        return dir;
    }

    public void setDir(Integer dir) {
        this.dir = dir;
    }

    public java.util.Date getFechaAlt() {
        return fechaAlt;
    }

    public void setFechaAlt(java.util.Date fechaAlt) {
        this.fechaAlt = fechaAlt;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Double getComision() {
        return comision;
    }

    public void setComision(Double comision) {
        this.comision = comision;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmpleadosEntity that = (EmpleadosEntity) o;
        return empNo == that.empNo && Objects.equals(apellido, that.apellido) && Objects.equals(oficio, that.oficio) && Objects.equals(dir, that.dir) && Objects.equals(fechaAlt, that.fechaAlt) && Objects.equals(salario, that.salario) && Objects.equals(comision, that.comision);
    }

    @Override
    public int hashCode() {
        return Objects.hash(empNo, apellido, oficio, dir, fechaAlt, salario, comision);
    }

    @Override
    public String toString() {
        return "EmpleadosEntity{" +
                "empNo=" + empNo +
                ", apellido='" + apellido + '\'' +
                ", oficio='" + oficio + '\'' +
                ", dir=" + dir +
                ", fechaAlt=" + fechaAlt +
                ", salario=" + salario +
                ", comision=" + comision;
    }

    public DepartamentosEntity getDepartamentosByDeptNo() {
        return departamentosByDeptNo;
    }

    public void setDepartamentosByDeptNo(DepartamentosEntity departamentosByDeptNo) {
        this.departamentosByDeptNo = departamentosByDeptNo;
    }
}
