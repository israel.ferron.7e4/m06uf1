package UF2.EA4.Menu;

import UF2.EA4.Metodes.Departament_CRUD;
import UF2.EA4.Metodes.Empleados_CRUD;

import java.io.IOException;
import java.util.Scanner;

public class MenuEA4 extends Menu {
    Empleados_CRUD emp;
    Departament_CRUD dep;

    public MenuEA4(Scanner scanner){
        super(scanner);
        emp = new Empleados_CRUD(scanner);
        dep = new Departament_CRUD(scanner);
        addOption("Exercicis UF1.EA4");
        addOption("Inserta aquests dos nous departaments a la taula departamentos");
        addOption("Inserta un nou empleat a cada departament nou, tria tu les dades dels nous empleats.");
        addOption("Actualitza el nom del departament número 20, ara es dirà RECERCA.");
        addOption("Actualitza el salari de l’empleat amb codi 7499, ara cobra 2100.");
        addOption("Elimina l’empleat que es diu SALA.");
        addOption("Fes una consulta per mostrar els empleats que cobrin més de 2000.");
    }

    @Override
    public void action(int select) {
        switch (select){
            case 1:
                dep.insert();
                break;
            case 2:
                emp.insert();
                break;
            case 3:
                dep.update();
                break;
            case 4:
                emp.update();
                break;
            case 5:
                emp.delete();
                break;
            case 6:
                emp.selectSalMoreThan2000();
                break;
        }
    }

}