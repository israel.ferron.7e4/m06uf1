package UF2.EA4.Metodes;

import UF2.EA3.Menu.Color;
import UF2.EA4.Clases.DepartamentosEntity;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;

import javax.persistence.Query;
import javax.persistence.metamodel.EntityType;
import java.util.Scanner;

public class Departament_CRUD {
    Scanner scanner;

    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure("hibernate_Hr.cfg.xml");

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public Departament_CRUD(Scanner scanner) {
        this.scanner = scanner;
    }

    public void insert() {
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();
        DepartamentosEntity dep1 = new DepartamentosEntity();
        DepartamentosEntity dep2 = new DepartamentosEntity();
        dep1.setDeptNo(60);
        dep1.setDnombre("TECNOLOGIA");
        dep1.setLoc("BARCELONA");
        dep2.setDeptNo(70);
        dep2.setDnombre("INFORMATICA");
        dep2.setLoc("SEVILLA");
        System.out.println(dep1);
        System.out.println(dep2);
        try {
            session.save(dep1);
            session.save(dep2);
            try {
                tx.commit();
            }catch (ConstraintViolationException e){
                System.out.println(Color.RED + "DEPARTAMENT DUPLICAT");
            }catch (Exception e){
                System.out.println(Color.RED + "ALTRES ERRORS");
            }
        }finally {
            System.out.println(Color.ITALIC + Color.PURPLE + "Dades Insertades Correctament\n" + Color.RESET);
            session.close();
        }
    }

    public void update(){
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();
        try {
            Metamodel metamodel = session.getSessionFactory().getMetamodel();
            for (EntityType<?> entityType : metamodel.getEntities()){
                String name = entityType.getName();
                Query query = session.createQuery("from "+ entityType.getName());
                for (Object o : query.getResultList()){
                    if (o instanceof DepartamentosEntity){
                        DepartamentosEntity dep = (DepartamentosEntity) o;
                        if (dep.getDeptNo() == 20){
                            dep.setDnombre("RESERCA");
                        }
                        session.save(dep);
                    }
                }
            }
        }finally {
            System.out.println(Color.ITALIC + Color.PURPLE + "Dades Modificades Correctament\n" + Color.RESET);
            tx.commit();
            session.close();
        }
    }
}
