package UF2.EA4.Metodes;

import UF2.EA3.Menu.Color;
import UF2.EA4.Clases.DepartamentosEntity;
import UF2.EA4.Clases.EmpleadosEntity;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;

import javax.persistence.Query;
import javax.persistence.metamodel.EntityType;
import java.util.Date;
import java.util.Scanner;

public class Empleados_CRUD {
    Scanner scanner;

    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure("hibernate_Hr.cfg.xml");

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public Empleados_CRUD(Scanner scanner) {
        this.scanner = scanner;
    }

    public void insert(){
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();
        EmpleadosEntity emp1 = new EmpleadosEntity();
        EmpleadosEntity emp2 = new EmpleadosEntity();
        DepartamentosEntity d = new DepartamentosEntity();
        emp1.setEmpNo(5555);
        emp1.setApellido("GIMENEZ");
        emp1.setDir(7944);
        emp1.setOficio("VENEDOR");
        emp1.setSalario((double) 2200);
        emp1.setComision((double) 55);
        d.setDeptNo(60);
        emp1.setDepartamentosByDeptNo(d);
        Date hoy = new Date();
        Date fecha = new Date(hoy.getTime());
        emp1.setFechaAlt(fecha);

        emp2.setEmpNo(4444);
        emp2.setApellido("RODRIGUEZ");
        emp2.setDir(7944);
        emp2.setOficio("VENEDOR");
        emp2.setSalario((double) 8600);
        emp2.setComision((double) 5);
        d.setDeptNo(70);
        emp2.setDepartamentosByDeptNo(d);
        hoy = new Date();
        fecha = new Date(hoy.getTime());
        emp2.setFechaAlt(fecha);
        try {
            session.save(emp1);
            session.save(emp2);
            try {
                tx.commit();
            }catch (ConstraintViolationException e){
                System.out.println(Color.RED + "DEPARTAMENT DUPLICAT");
            }catch (TransientPropertyValueException e){
                System.out.println(Color.RED + "DEPARTAMENT INEXISTENT");
            }catch (Exception e){
                System.out.println(Color.RED + "ALTRES ERRORS");
            }
        }finally {
            System.out.println(Color.ITALIC + Color.PURPLE + "Dades Insertades Correctament\n" + Color.RESET);
            session.close();
        }
    }

    public void update(){
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();
        try {
            Metamodel metamodel = session.getSessionFactory().getMetamodel();
            for (EntityType<?> entityType : metamodel.getEntities()){
                Query query = session.createQuery("from "+ entityType.getName());
                for (Object o : query.getResultList()){
                    if (o instanceof EmpleadosEntity){
                        EmpleadosEntity emp = (EmpleadosEntity) o;
                        if (emp.getEmpNo() == 7499){
                            emp.setSalario((double) 2100);
                        }
                        session.save(emp);
                    }
                }
            }
        }finally {
            System.out.println(Color.ITALIC + Color.PURPLE + "Dades Modificades Correctament\n" + Color.RESET);
            tx.commit();
            session.close();
        }
    }

    public void delete(){
        final Session session = getSession();
        final Transaction tx = session.beginTransaction();
        try {
            Metamodel metamodel = session.getSessionFactory().getMetamodel();
            for (EntityType<?> entityType : metamodel.getEntities()){
                Query query = session.createQuery("from " + entityType.getName());
                for (Object o : query.getResultList()){
                    if (o instanceof EmpleadosEntity){
                        EmpleadosEntity emp = (EmpleadosEntity) o;
                        if (emp.getApellido().equals("SALA")){
                            session.delete(emp);
                        }
                    }
                }
            }
        }finally {
            System.out.println(Color.ITALIC + Color.PURPLE + "Dades Eliminades Correctament\n" + Color.RESET);
            tx.commit();
            session.close();
        }
    }

    public void selectSalMoreThan2000(){
        final Session session = getSession();
        try {
            Metamodel metamodel = session.getSessionFactory().getMetamodel();
            for (EntityType<?> entityType : metamodel.getEntities()){
                Query query = session.createQuery("from " + entityType.getName());
                for (Object o : query.getResultList()){
                    if (o instanceof EmpleadosEntity){
                        EmpleadosEntity emp = (EmpleadosEntity) o;
                        if (emp.getSalario() > 2000){
                            System.out.println(emp);
                        }
                    }
                }
            }
        }finally {
            session.close();
        }
    }
}
