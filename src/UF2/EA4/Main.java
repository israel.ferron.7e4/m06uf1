package UF2.EA4;

import UF2.EA4.Menu.Color;
import UF2.EA4.Menu.MenuEA4;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuEA4 menuEA4 = new MenuEA4(scanner);
        menuEA4.showMenu();

        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!" + Color.RESET);
    }
}
