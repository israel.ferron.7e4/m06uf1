package UF2.EA1.Metodes;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetodesEA1 {
    Scanner scanner;

    public MetodesEA1(Scanner scanner) {
        this.scanner = scanner;
    }

    public void Ex1() throws ClassNotFoundException, SQLException {
        Connection conexion = DriverManager.getConnection
                ("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");

        Statement query = conexion.createStatement();
        String selAlumn = "SELECT * from alumnos";
        boolean valor = query.execute(selAlumn);

        if (valor) {
            ResultSet rs = query.getResultSet();
            int n = 1;
            while (rs.next()) {
                System.out.printf("Alumno  %s %nID:%s Nom:%s Dir:%s Pueblo:%s Tel:%s \n", n, rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                n++;
            }
        } else {
            int f = query.getUpdateCount();
            System.out.printf("Filas afectadas:%d \n", f);
        }

        query.close();
        conexion.close();
    }

    public void Ex2() throws ClassNotFoundException, SQLException {
        Connection conexion = DriverManager.getConnection
                ("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");

        Statement query = conexion.createStatement();
        String selNota = "SELECT * from notas";
        boolean valor = query.execute(selNota);

        if (valor) {
            ResultSet rs = query.getResultSet();
            int n = 1;
            while (rs.next()) {
                System.out.printf("Nota %d %nDNI:%s Cod:%s Nota:%s\n", n, rs.getString(1), rs.getString(2), rs.getString(3));
                n++;
            }
        } else {
            int f = query.getUpdateCount();
            System.out.printf("Filas afectadas:%d \n", f);
        }

        query.close();
        conexion.close();
    }

    public void Ex3() throws SQLException {
        Connection conexion = DriverManager.getConnection
                ("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");


        try {
            String query = "SELECT * FROM notas WHERE dni like ?";
            PreparedStatement pst = conexion.prepareStatement(query);
            pst.setString(1, "4448242");

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                System.out.println("DNI: - " + rs.getInt("dni") +
                        ", Codigo " + rs.getString("cod") +
                        ", Nota " + rs.getString("nota"));
            }
            rs.close();
            pst.close();
        } catch (SQLException ex) {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
        }
    }

    public void Ex4() {
        try {
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");

            Statement stmt = conexion.createStatement();
            String insert = "INSERT INTO ALUMNOS VALUES ('12325311','Martinez, Roc', 'Av.Once de Semtembre','Barcelona','935078899')";
            stmt.executeUpdate(insert);
            stmt.close();
            Statement stmt2 = conexion.createStatement();
            String insert2 = "INSERT INTO ALUMNOS VALUES ('12325315','Julian, Roc', 'Calle Estrella','Madrid','985578899')";
            stmt2.executeUpdate(insert2);
            stmt2.close();
            Statement stmt3 = conexion.createStatement();
            String insert3 = "INSERT INTO Alumnos VALUES ('12325351','Ferron , Roc', 'C/ Estopenya, 24','Murcia','935558899')";
            stmt3.executeUpdate(insert3);
            stmt3.close();
            conexion.close();
            System.out.println("Insertades correctament les dades");
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }
    }

    public void Ex5() {
        try {
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");
            String dni;
            int cod;
            cod = 4;
            dni = "12325311";
            String query = "INSERT INTO notas VALUES "
                    + "( ?, ?, ? )";
            PreparedStatement ps1 =
                    conexion.prepareStatement(query);
            ps1.setString(1, dni);
            ps1.setInt(2, cod);
            ps1.setInt(3, 8);
            ps1.executeUpdate();
            ps1.close();

            cod = 5;
            PreparedStatement ps2 =
                    conexion.prepareStatement(query);
            ps2.setString(1, dni);
            ps2.setInt(2, cod);
            ps2.setInt(3, 8);
            ps2.executeUpdate();
            ps2.close();

            cod = 4;
            dni = "12325315";
            PreparedStatement ps3 =
                    conexion.prepareStatement(query);
            ps3.setString(1, dni);
            ps3.setInt(2, cod);
            ps3.setInt(3, 8);
            ps3.executeUpdate();
            ps3.close();

            cod = 5;
            PreparedStatement ps4 =
                    conexion.prepareStatement(query);
            ps4.setString(1, dni);
            ps4.setInt(2, cod);
            ps4.setInt(3, 8);
            ps4.executeUpdate();
            ps4.close();

            cod = 4;
            dni = "12325351";
            PreparedStatement ps5 =
                    conexion.prepareStatement(query);
            ps5.setString(1, dni);
            ps5.setInt(2, cod);
            ps5.setInt(3, 8);
            ps5.executeUpdate();
            ps5.close();

            cod = 5;
            PreparedStatement ps6 =
                    conexion.prepareStatement(query);
            ps6.setString(1, dni);
            ps6.setInt(2, cod);
            ps6.setInt(3, 8);
            ps6.executeUpdate();
            ps6.close();

            conexion.close(); // Cerrar conexi�n
            System.out.println("Les dades estan Insetades");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex6(){
        try {
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");

            String query = "UPDATE NOTAS SET NOTA = 9 WHERE DNI = '4448242'";

            Statement sentencia = conexion.createStatement();
            try {
                sentencia.executeUpdate(query.toString());

            } catch (SQLException e) {
                //e.printStackTrace();
                System.out.printf("HA OCURRIDO UNA EXCEPCI�N:%n");
                System.out.printf("Mensaje   : %s %n", e.getMessage());
                System.out.printf("SQL estado: %s %n", e.getSQLState());
                System.out.printf("C�d error : %s %n", e.getErrorCode());
            }

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n
            System.out.println("Dades actulizades");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex7(){
        String dni = "12344345", telefono = "934885237";
        try {
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");

            String query = "UPDATE alumnos SET telef = '934885237' WHERE dni = '12344345'";

            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(query);

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n
            System.out.println("Dades actulizades");

         }catch (SQLException e) {
            if (e.getErrorCode() == 1062)
                System.out.println("CLAVE PRIMARIA DUPLICADA");
            else
            if (e.getErrorCode() == 1452)
                System.out.println("CLAVE AJENA "+ dni + " NO EXISTE");

            else {
                System.out.println("HA OCURRIDO UNA EXCEPCI�N:");
                System.out.println("Mensaje:    " + e.getMessage());
                System.out.println("SQL estado: " + e.getSQLState());
                System.out.println("C�d error:  " + e.getErrorCode());
            }
        }
    }

    public void Ex8(){
        try {
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");

            String query = "delete from alumnos where pobla like 'Mostoles'";

            Statement sentencia = conexion.createStatement();
            try {
                sentencia.execute(query.toString());

            } catch (SQLException e) {
                //e.printStackTrace();
                System.out.printf("HA OCURRIDO UNA EXCEPCI�N:%n");
                System.out.printf("Mensaje   : %s %n", e.getMessage());
                System.out.printf("SQL estado: %s %n", e.getSQLState());
                System.out.printf("C�d error : %s %n", e.getErrorCode());
            }

            sentencia.close(); // Cerrar Statement
            conexion.close(); // Cerrar conexi�n
            System.out.println("Dades Eliminades");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
