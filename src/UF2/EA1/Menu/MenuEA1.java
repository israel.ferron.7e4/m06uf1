package UF2.EA1.Menu;

import UF2.EA1.Metodes.MetodesEA1;

import java.sql.SQLException;
import java.util.Scanner;

public class MenuEA1 extends Menu {
    MetodesEA1 metodesEA1;

    public MenuEA1(Scanner scanner){
        super(scanner);
        metodesEA1 = new MetodesEA1(scanner);
        addOption("EA1 UF2");
        addOption("Mostrar les dades de la taula ALUMNOS seguint l’exemple de la classe MostraAsignaturas.");
        addOption("Mostrar totes les dades de la taula NOTAS seguint l’exemple de la classe MostraAsignaturas.");
        addOption("Mostrar les notes de l’alumne amb DNI «4448242» de la taula NOTAS seguint l'exemple de la classe\n" +
                "\"ConsultaPreparedStatement.java\"");
        addOption("Insertar 3 alumnes nous. Inventat les dades dels alumnes nous. Segueix l'exemple de la classe \"Insert.java\"");
        addOption("Insertar les notes per aquests 3 nous alumnes de les assignatures FOL i RET. Tots han tret un 8 en les dues\n" +
                "assignatures. Utilitza la Classe de JDBC \"PreparedStatement\" seguint l'exemple de la classe\n" +
                "\"InsertaDepPreparedStatement.java\".");
        addOption("Modificar les notes de l’alumne \"Cerrato Vela, Luis\" de FOL i RET, ha tret un 9. Segueix l'exemple de la\n" +
                "classe \"Update.java\"");
        addOption("Modificar el teléfon utilitzant paràmetres de l’alumne amb DNI = 12344345, el nou teléfon és 934885237.\n" +
                "Segueix l'exemple de la classe \"ModificarSalario.java\".");
        addOption("Eliminar l’alumne que viu a \"Mostoles\".");
    }

    @Override
    public void action(int select) throws SQLException, ClassNotFoundException {
        switch (select){
            case 1:
                metodesEA1.Ex1();
                break;
            case 2:
                metodesEA1.Ex2();
                break;
            case 3:
                metodesEA1.Ex3();
                break;
            case 4:
                metodesEA1.Ex4();
                break;
            case 5:
                metodesEA1.Ex5();
                break;
            case 6:
                metodesEA1.Ex6();
                break;
            case 7:
                metodesEA1.Ex7();
                break;
            case 8:
                metodesEA1.Ex8();
                break;

        }
    }

}
