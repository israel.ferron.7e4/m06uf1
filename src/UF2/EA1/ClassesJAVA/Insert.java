package UF2.EA1.ClassesJAVA;

import java.lang.*;
import java.sql.*;

public class Insert {
    public static void main(String[] args){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/school","user1","pass1");
            Statement stmt = conn.createStatement();
            String insert = "INSERT IGNORE INTO ALUMNOS VALUES ('12325311','Colomer Vila, Roc', 'C/ Estopenya, 24','Barcelona','935578899')";
            stmt.executeUpdate(insert);
			stmt.close();
			conn.close();
        }catch(SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }catch(Exception ex){
            System.out.println("Exception: " + ex.getMessage());
        }
    }
                                
}


