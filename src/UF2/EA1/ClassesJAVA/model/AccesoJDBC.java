/****************************************************************
* Libro RA-MA: Acceso a Datos (2013)
* Jose E. Córcoles y Francisco Montero 
* 
* */
package UF2.EA1.ClassesJAVA.model;

/**
 *
 * @author Jose E. Córcoles y Francisco Montero
 */
public class AccesoJDBC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //EN ESTE MAIN SE INVOCAN A DIFERENTES MÉTODOS SEGÚN LO QUE SE QUIERA HACER. REALMENTE NO ES
        //UNA APLICACIÓN, SOLO UN BENCHMARK PARA PROBAR LO VISTO EN EL CAPÍTULO.
        //RECUERDA:
        // * deben estar instalados los drivers MySQL en NetBeans para que funcione
        // * Se deben crear la estructura de tablas indicadas en el CAP2 apra que funcione.
        // * Se debe dar acceso a MySQL con usuario "root" y clave "" para que funcione
        Gestor_conexion gestor = new Gestor_conexion();
        
        //USALO PARA: probar el Pool de conexiones
        //DataSourceExample dse = new DataSourceExample();
        //dse.crearConexion();
        
        //USALO PARA: INSERTAR UN REGISTRO NUEVO
        //gestor.Insertar();
        //USALO PARA: EJECUTAR UNA CONSULTA SELECT
        gestor.Consulta_Statement();
        //USALO PARA: EJECUTAR UNA CONSULTA SELECT CON PARÁMETROS
        gestor.Consulta_preparedStatement();
         //USALO PARA: INSERTAR UN REGISTRO NUEVO CON TRANSACCIONES
        // gestor.Insertar_con_commit();   
        gestor.cerrar_Conexion();
    }
}
