/****************************************************************
* Libro RA-MA: Acceso a Datos (2013)
* Jose E. Córcoles y Francisco Montero 
* 
* */
package UF2.EA1.ClassesJAVA.model;

//import com.mysql.jdbc.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Jose E. Córcoles y Francisco Montero
 */
public class Gestor_conexion {

    Connection conn1 = null;
    public Gestor_conexion() {
         // ABRE UNA CONEXIÓN A UNA BASE DE DATOS QUE SE SUPONE MYSQL Y QUE TIENE LAS TABLAS
        // Y LOS USUARIOS CREADOS SEGÚN ESTE EJEMPLO.
        
 
        try {
            //RECUERDA: PARA EJECUTAR ESTE CÓDIGO ES NECESARIO TENER mYSQL FUNCIONANDO Y LAS TABLAS Y USUARIOS CREADOS
            String url1 = "jdbc:mysql://localhost:3306/discografica";
            String user = "root";
            String password = "";
            conn1 = DriverManager.getConnection(url1, user, password);
            if (conn1 != null) {
                System.out.println("Conectado discografica");
            }
 
            
        } catch (SQLException ex) {
            System.out.println("ERROR:La dirección no es válida o el usuario y clave");
            ex.printStackTrace();
        }
    }
    public void Insertar(){
        try {
            
            // CREA UN STATEMENT  PARA UNA CONSULTA SQL INSERT.
            Statement sta = conn1.createStatement();
            sta.executeUpdate("INSERT INTO album " + "VALUES (3, 'Black Album', 'Metallica')");
            sta.close();
        } catch (SQLException ex) {
            System.out.println("ERROR:al hacer un Insert");
            
            ex.printStackTrace();
        }
    }
    public void Insertar_con_commit(){
        //INSERTA CON UN SQL INSERT PERO RESPETANDO TRANSACCIONES
        try {
            conn1.setAutoCommit(false);
            // create our java jdbc statement
            Statement sta = conn1.createStatement();
            sta.executeUpdate("INSERT INTO album " + "VALUES (5, 'Black Album', 'Metallica')");
            sta.executeUpdate("INSERT INTO album " + "VALUES (6, 'A kind of magic', 'Queen')");
            conn1.commit();
        } catch (SQLException ex) {
            System.out.println("ERROR:al hacer un Insert");
            try{
		 if(conn1!=null)  conn1.rollback();
            }catch(SQLException se2){
              se2.printStackTrace();
      }//end try
            ex.printStackTrace();
        }
    }
    public void Consulta_Statement(){
      //CRAR UN ESTATEMENT PARA UNA CONSULTA SELECT
        try {
        Statement stmt = conn1.createStatement();
        String query = "SELECT * FROM album WHERE titulo like 'B%'";
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            System.out.println("ID - " + rs.getInt("id") + 
                ", Título " + rs.getString("titulo") + 
                ", Autor " + rs.getString("autor") );
        }
        rs.close();
        stmt.close();
      } catch (SQLException ex) {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
      }

    }
    public void Consulta_preparedStatement(){
        //CRAR UN ESTATEMENT PARA UNA CONSULTA SELECT CON PARÁMETROS
      try {
        String query = "SELECT * FROM album WHERE titulo like ?";
        PreparedStatement pst = conn1.prepareStatement(query);
        pst.setString(1, "B%");
        
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
            System.out.println("ID - " + rs.getInt("id") + 
                ", Título " + rs.getString("titulo") + 
                ", Autor " + rs.getString("autor") );
        }
        rs.close();
        pst.close();
      } catch (SQLException ex) {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
      }

    }
    public void cerrar_Conexion (){
       //SE CIERRA LA CONEXIÓN
        try {
        
        conn1.close();
        
      } catch (SQLException ex) {
            System.out.println("ERROR:al cerrar la conexión");
            ex.printStackTrace();
      }
    }
    
    
    
}
