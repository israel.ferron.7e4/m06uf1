package UF2.EA2.Menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

abstract class Menu {
    List<String> options;
    Scanner scanner;

    public Menu(Scanner scanner) {
        this.options = new ArrayList<>();
        this.scanner = scanner;
    }

    public void addOption(String option){
        options.add(option);
    }

    private int selectOption(){
        System.out.println("Introdueix l'opció: ");
        int n = scanner.nextInt();
        while (n < 0 || n > options.size()){
            System.out.println( Color.UNDER_LINE + Color.BOLD + Color.RED + "Opcion invalida" + Color.RESET);
            System.out.print("Introdueix l'opció: ");
            n = scanner.nextInt();
        }
        return n;
    }

    public void showMenu() throws IOException {
        while (true){
            System.out.print(Color.CYAN + Color.BOLD + Color.ITALIC);
            System.out.println(options.get(0));
            System.out.print(Color.RESET);
            for (int i = 1; i < options.size(); i++){
                System.out.print(Color.ITALIC + Color.GREEN);
                System.out.printf("%d) %s\n",i,options.get(i));
            }
            System.out.print(Color.RED);
            System.out.println("0) Sortir");
            System.out.print(Color.RESET);
            int select = selectOption();
            if (select == 0)
                return;
            else
                action(select);
        }
    }

    public abstract void action(int select) throws IOException;
}
