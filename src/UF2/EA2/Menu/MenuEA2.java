package UF2.EA2.Menu;

import UF2.EA2.Metodes.MetodesEA2;

import java.io.IOException;
import java.util.Scanner;

public class MenuEA2 extends Menu {
    MetodesEA2 metodesEA2;

    public MenuEA2(Scanner scanner){
        super(scanner);
        metodesEA2 = new MetodesEA2(scanner);
        addOption("Exercicis UF1.EA2 UF1");
        addOption("Executar Script school2.sql");
        addOption("Mostrar informació de la BBDD school2");
        addOption("Mostra la informació dels camps de la taula alumnes");
        addOption("Mostra la informació dels camps de la taula asignatures");
        addOption("Mostra la informació dels camps de la taula notas");
        addOption("Mostra la informació dels camps de la taula alumnes");
        addOption("Mostra la informació dels camps de la taula asignaturas");
        addOption("Mostra la informació dels camps de la taula notas");
        addOption("Mostra les claus primaries de la taula alumnes");
        addOption("Mostra les claus primaries de la taula asignatiras");
        addOption("Mostra les claus primaries de la taula notas");
        addOption("Mostra les claus foràneas de la taula alumnes");
        addOption("Mostra les claus foràneas de la taula asignaturas");
        addOption("Mostra les claus foràneas de la taula notas");
    }

    @Override
    public void action(int select) throws IOException {
        switch (select){
            case 1:
                metodesEA2.Ex1();
                break;
            case 2:
                metodesEA2.Ex2();
                break;
            case 3:
                metodesEA2.Ex3();
                break;
            case 4:
                metodesEA2.Ex4();
                break;
            case 5:
                metodesEA2.Ex5();
                break;
            case 6:
                metodesEA2.Ex6();
                break;
            case 7:
                metodesEA2.Ex7();
                break;
            case 8:
                metodesEA2.Ex8();
                break;
            case 9:
                metodesEA2.Ex9();
                break;
            case 10:
                metodesEA2.Ex10();
                break;
            case 11:
                metodesEA2.Ex11();
                break;
            case 12:
                metodesEA2.Ex12();
                break;
            case 13:
                metodesEA2.Ex13();
                break;
            case 14:
                metodesEA2.Ex14();
                break;
        }
    }

}
