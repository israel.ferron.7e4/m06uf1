package UF2.EA2.Metodes;

import UF2.EA2.Menu.Color;

import java.io.*;
import java.sql.*;
import java.util.Scanner;

public class MetodesEA2 {
    Scanner scanner;

    public MetodesEA2(Scanner scanner) {
        this.scanner = scanner;
    }

    public void Ex1(){
        File file = new File("src/UF2/EA2/school2.sql");
        System.out.println("\n\nFichero de consulta : " + file.getName());
        System.out.println("Convirtiendo el fichero a cadena...");
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            System.out.println("ERROR NO HAY FILE: " + e.getMessage());
        }
        String linea = null;
        StringBuilder stringBuilder = new StringBuilder();
        String salto = System.getProperty("line.separator");
        try {
            while ((linea = entrada.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }
        } catch (IOException e) {
            System.out.println("ERROR de E/S, al operar " + e.getMessage());
        }
        String consulta = stringBuilder.toString();
        System.out.println(consulta);
        try {
            Connection connmysql = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");
            Statement sents = connmysql.createStatement();
            int res = sents.executeUpdate(consulta);
            System.out.println("Script creado con éxito, res = " + res);
            connmysql.close();
            sents.close();
        } catch (SQLException e) {
            System.out.println("ERROR AL EJECUTAR EL SCRIPT: " + e.getMessage());
        }

    }

    public void Ex2(){
        try
        {
            //Establecemos la conexion con la BD
            Connection connmysql = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");


            DatabaseMetaData dbmd = connmysql.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            String nombre  = dbmd.getDatabaseProductName();
            String driver  = dbmd.getDriverName();
            String url     = dbmd.getURL();
            String usuario = dbmd.getUserName() ;

            System.out.println("INFORMACI�N SOBRE LA BASE DE DATOS:");
            System.out.println("===================================");
            System.out.printf("Nombre : %s %n", nombre );
            System.out.printf("Driver : %s %n", driver );
            System.out.printf("URL    : %s %n", url );
            System.out.printf("Usuario: %s %n", usuario );

            //Obtener informaci�n de las tablas y vistas que hay
            resul = dbmd.getTables(null, "ejemplo", null, null);

            while (resul.next()) {
                String catalogo = resul.getString(1);//columna 1
                String esquema = resul.getString(2); //columna 2
                String tabla = resul.getString(3);   //columna 3
                String tipo = resul.getString(4);	  //columna 4
                System.out.printf("%s - Catalogo: %s, Esquema: %s, Nombre: %s %n",
                        tipo, catalogo, esquema, tabla);
            }
            connmysql.close(); //Cerrar conexion
        }
        catch (SQLException e) {e.printStackTrace();}
    }

    public void Ex3(){
        try {

            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");

            Statement sentencia = conexion.createStatement();
            ResultSet rs = sentencia
                    .executeQuery("SELECT * FROM ALUMNOS");

            ResultSetMetaData rsmd = rs.getMetaData();

            int nColumnas = rsmd.getColumnCount();
            String nula;
            System.out.printf("N�mero de columnas recuperadas: %d%n", nColumnas);
            for (int i = 1; i <= nColumnas; i++) {
                System.out.printf("Columna %d: %n ", i);
                System.out.printf("  Nombre: %s %n   Tipo: %s %n ",
                        rsmd.getColumnName(i),  rsmd.getColumnTypeName(i));
                if (rsmd.isNullable(i) == 0)
                    nula = "NO";
                else
                    nula = "SI";

                System.out.printf("  Puede ser nula?: %s %n ", nula);
                System.out.printf("  M�ximo ancho de la columna: %d %n",
                        rsmd.getColumnDisplaySize(i));
            }// for

            sentencia.close();
            rs.close();
            conexion.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex4(){
        try {

            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");

            Statement sentencia = conexion.createStatement();
            ResultSet rs = sentencia
                    .executeQuery("SELECT * FROM asignaturas");

            ResultSetMetaData rsmd = rs.getMetaData();

            int nColumnas = rsmd.getColumnCount();
            String nula;
            System.out.printf("N�mero de columnas recuperadas: %d%n", nColumnas);
            for (int i = 1; i <= nColumnas; i++) {
                System.out.printf("Columna %d: %n ", i);
                System.out.printf("  Nombre: %s %n   Tipo: %s %n ",
                        rsmd.getColumnName(i),  rsmd.getColumnTypeName(i));
                if (rsmd.isNullable(i) == 0)
                    nula = "NO";
                else
                    nula = "SI";

                System.out.printf("  Puede ser nula?: %s %n ", nula);
                System.out.printf("  M�ximo ancho de la columna: %d %n",
                        rsmd.getColumnDisplaySize(i));
            }// for

            sentencia.close();
            rs.close();
            conexion.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex5(){
        try {

            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");

            Statement sentencia = conexion.createStatement();
            ResultSet rs = sentencia
                    .executeQuery("SELECT * FROM notas");

            ResultSetMetaData rsmd = rs.getMetaData();

            int nColumnas = rsmd.getColumnCount();
            String nula;
            System.out.printf("N�mero de columnas recuperadas: %d%n", nColumnas);
            for (int i = 1; i <= nColumnas; i++) {
                System.out.printf("Columna %d: %n ", i);
                System.out.printf("  Nombre: %s %n   Tipo: %s %n ",
                        rsmd.getColumnName(i),  rsmd.getColumnTypeName(i));
                if (rsmd.isNullable(i) == 0)
                    nula = "NO";
                else
                    nula = "SI";

                System.out.printf("  Puede ser nula?: %s %n ", nula);
                System.out.printf("  M�ximo ancho de la columna: %d %n",
                        rsmd.getColumnDisplaySize(i));
            }// for

            sentencia.close();
            rs.close();
            conexion.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex6(){
        try
        {
            //Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("COLUMNAS TABLA ALUMNOS:");
            System.out.println("===================================");
            ResultSet columnas=null;
            columnas = dbmd.getColumns("school2", "public", "alumnos", null);
            while (columnas.next()) {
                String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                String nula  = columnas.getString("IS_NULLABLE");   //getString(18)

                System.out.printf("Columna: %s, Tipo: %s, Tama�o: %s, �Puede ser Nula:? %s %n", nombCol, tipoCol, tamCol, nula);
            }


            conexion.close(); //Cerrar conexion
        }
        catch (SQLException e) {e.printStackTrace();}
    }//fin de main

    public void Ex7(){
        try
        {
            //Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("COLUMNAS TABLA ASIGNATURAS:");
            System.out.println("===================================");
            ResultSet columnas=null;
            columnas = dbmd.getColumns("school2", "public", "asignaturas", null);
            while (columnas.next()) {
                String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                String nula  = columnas.getString("IS_NULLABLE");   //getString(18)

                System.out.printf("Columna: %s, Tipo: %s, Tama�o: %s, �Puede ser Nula:? %s %n", nombCol, tipoCol, tamCol, nula);
            }


            conexion.close(); //Cerrar conexion
        }
        catch (SQLException e) {e.printStackTrace();}
    }//fin de main

    public void Ex8(){
        try
        {
            //Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("COLUMNAS TABLA NOTAS:");
            System.out.println("===================================");
            ResultSet columnas=null;
            columnas = dbmd.getColumns("school2", "public", "notas", null);
            while (columnas.next()) {
                String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                String nula  = columnas.getString("IS_NULLABLE");   //getString(18)

                System.out.printf("Columna: %s, Tipo: %s, Tama�o: %s, �Puede ser Nula:? %s %n", nombCol, tipoCol, tamCol, nula);
            }


            conexion.close(); //Cerrar conexion
        }
        catch (SQLException e) {e.printStackTrace();}
    }//fin de main

    public void Ex9(){
        try {

            //Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("CLAVE PRIMARIA TABLA ALUMNOS:");
            System.out.println("===================================");
            ResultSet pk = dbmd.getPrimaryKeys("school2", "public", "alumnos");
            String pkDep="", separador="";
            while (pk.next()) {
                pkDep = pkDep + separador +
                        pk.getString("COLUMN_NAME");//getString(4)
                separador="+";
            }
            System.out.println("Clave Primaria: " + pkDep);



            conexion.close(); //Cerrar conexion
        }
        catch (SQLException e) {e.printStackTrace();}
    }//fin de main

    public void Ex10(){
        try {

            //Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("CLAVE PRIMARIA TABLA ASIGNATURAS:");
            System.out.println("===================================");
            ResultSet pk = dbmd.getPrimaryKeys("school2", "public", "asignaturas");
            String pkDep="", separador="";
            while (pk.next()) {
                pkDep = pkDep + separador +
                        pk.getString("COLUMN_NAME");//getString(4)
                separador="+";
            }
            System.out.println("Clave Primaria: " + pkDep);



            conexion.close(); //Cerrar conexion
        }
        catch (SQLException e) {e.printStackTrace();}
    }//fin de main

    public void Ex11(){
        try {

            //Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("CLAVE PRIMARIA TABLA NOTAS:");
            System.out.println("===================================");
            ResultSet pk = dbmd.getPrimaryKeys("school2", "public", "notas");
            String pkDep="", separador="";
            while (pk.next()) {
                pkDep = pkDep + separador +
                        pk.getString("COLUMN_NAME");//getString(4)
                separador="+";
            }
            System.out.println("Clave Primaria: " + pkDep);



            conexion.close(); //Cerrar conexion
        }
        catch (SQLException e) {e.printStackTrace();}
    }//fin de main

    public void Ex12(){
        try {
            // Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");


            DatabaseMetaData dbmd = conexion.getMetaData();// Creamos
            // objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("CLAVES ajenas que referencian a ALUMNOS:");
            System.out.println("==============================================");

            ResultSet fk = dbmd.getExportedKeys("school2", "public", "alumnos");

            while (fk.next()) {
                String fk_name = fk.getString("FKCOLUMN_NAME");
                String pk_name = fk.getString("PKCOLUMN_NAME");
                String pk_tablename = fk.getString("PKTABLE_NAME");
                String fk_tablename = fk.getString("FKTABLE_NAME");
                System.out.printf("Tabla PK: %s, Clave Primaria: %s %n", pk_tablename, pk_name);
                System.out.printf("Tabla FK: %s, Clave Ajena: %s %n", fk_tablename, fk_name);
            }

            conexion.close(); // Cerrar conexion
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex13(){
        try {

            // Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");


            DatabaseMetaData dbmd = conexion.getMetaData();// Creamos
            // objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("CLAVES ajenas que referencian a ASIGNATURAS:");
            System.out.println("==============================================");

            ResultSet fk = dbmd.getExportedKeys("school2", "public", "asignaturas");

            while (fk.next()) {
                String fk_name = fk.getString("FKCOLUMN_NAME");
                String pk_name = fk.getString("PKCOLUMN_NAME");
                String pk_tablename = fk.getString("PKTABLE_NAME");
                String fk_tablename = fk.getString("FKTABLE_NAME");
                System.out.printf("Tabla PK: %s, Clave Primaria: %s %n", pk_tablename, pk_name);
                System.out.printf("Tabla FK: %s, Clave Ajena: %s %n", fk_tablename, fk_name);
            }

            conexion.close(); // Cerrar conexion
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Ex14(){
        try {

            // Establecemos la conexion con la BD
            Connection conexion = DriverManager.getConnection
                    ("jdbc:postgresql://192.168.56.101:5432/school2", "school2", "school2");


            DatabaseMetaData dbmd = conexion.getMetaData();// Creamos
            // objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("CLAVES ajenas que referencian a NOTAS:");
            System.out.println("==============================================");

            ResultSet fk = dbmd.getExportedKeys("school2", "public", "notas");

            while (fk.next()) {
                String fk_name = fk.getString("FKCOLUMN_NAME");
                String pk_name = fk.getString("PKCOLUMN_NAME");
                String pk_tablename = fk.getString("PKTABLE_NAME");
                String fk_tablename = fk.getString("FKTABLE_NAME");
                System.out.printf("Tabla PK: %s, Clave Primaria: %s %n", pk_tablename, pk_name);
                System.out.printf("Tabla FK: %s, Clave Ajena: %s %n", fk_tablename, fk_name);
            }

            conexion.close(); // Cerrar conexion
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}