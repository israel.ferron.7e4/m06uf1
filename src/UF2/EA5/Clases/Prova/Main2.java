//import Model.DepartamentosEntity;
//import org.hibernate.*;
//import org.hibernate.cfg.Configuration;
//public class Main2 {
//    private static final SessionFactory ourSessionFactory;
//    static {
//        try {
//            Configuration configuration = new Configuration();
//            configuration.configure();
//            ourSessionFactory = configuration.buildSessionFactory();
//        } catch (Throwable ex) {
//            throw new ExceptionInInitializerError(ex);
//        }
//    }
//    public static Session getSession() throws HibernateException {
//        return ourSessionFactory.openSession();
//    }
//    public static void main(final String[] args) {
//        final Session session = getSession();
//        final Transaction tx = session.beginTransaction();
//        try {
//            DepartamentosEntity  dep = (DepartamentosEntity) session.load(DepartamentosEntity.class, 40);
//            System.out.printf("Nom antic: %s%n", dep.getDnombre());
//            dep.setDnombre("RECERCA");
//            session.update(dep); // modifica el departament a la taula
//            tx.commit();
//            System.out.printf("Nom nou: %s%n", dep.getDnombre());
//
//        } catch (ObjectNotFoundException o) {
//            System.out.println("NO EXISTE EL DEPARTAMENTO...");
//        } catch (Exception e) {
//            System.out.println("ERROR NO CONTROLADO....");
//            e.printStackTrace();
//        }
//        session.close();
//        System.exit(0);
//    }
//}