//import Model.DepartamentosEntity;
//import Model.EmpleadosEntity;
//import org.hibernate.*;
//import org.hibernate.cfg.Configuration;
//import org.hibernate.exception.ConstraintViolationException;
//import java.math.BigDecimal;
//
//public class Main3 {
//    private static final SessionFactory ourSessionFactory;
//
//    static {
//        try {
//            Configuration configuration = new Configuration();
//            configuration.configure();
//
//            ourSessionFactory = configuration.buildSessionFactory();
//        } catch (Throwable ex) {
//            throw new ExceptionInInitializerError(ex);
//        }
//    }
//
//    public static Session getSession() throws HibernateException {
//        return ourSessionFactory.openSession();
//    }
//
//    public static void main(String[] args) {
//        final Session session = getSession();
//        final Transaction tx = session.beginTransaction();
//        System.out.println("Inserto un EMPLEADO EN EL DEPARTAMENTO 10.");
//        BigDecimal salario =  new BigDecimal(1600);// inicializo el salario
//        BigDecimal comision = new BigDecimal(20); // inicializo la comisión
//        EmpleadosEntity em = new EmpleadosEntity(); // creo un objeto empleados
//        em.setEmpNo((short) 4466); // el numero de empleado es 4466
//        em.setApellido("RAMON");
//        em.setDir(7497); // el director es el numero de empleado 7499
//        em.setOficio("VENEDOR");
//        em.setSalario(salario);
//        em.setComision(comision);
//
//        //carreguem el Departament 10
//        DepartamentosEntity  dep = (DepartamentosEntity) session.load(DepartamentosEntity.class, 10);
//        em.setDepartamentosByDeptNo(dep);
//
//        // fecha de alta
//        java.util.Date hoy = new java.util.Date();
//        java.sql.Date fecha = new java.sql.Date(hoy.getTime());
//        em.setFechaAlt(fecha);
//        try {
//            session.save(em);
//            try {
//                tx.commit();
//            } catch (ConstraintViolationException e) {
//                System.out.println("EMPLEADO DUPLICADO");
//                System.out.printf("MENSAJE: %s%n", e.getMessage());
//                System.out.printf("COD ERROR: %d%n", e.getErrorCode());
//                System.out.printf("ERROR SQL: %s%n", e.getSQLException().getMessage());
//            }
//        } catch (TransientPropertyValueException e) {
//            System.out.println("EL DEPARTAMENTO NO EXISTE");
//            System.out.printf("MENSAJE: %s%n", e.getMessage());
//            System.out.printf("Propiedad: %s%n", e.getPropertyName());
//        } catch (Exception e) {
//            System.out.println("ERROR NO CONTROLADO....");
//            e.printStackTrace();
//        }
//        session.close();
//        System.exit(0);
//    }
//}