//import Model.DepartamentosEntity;
//import Model.EmpleadosEntity;
//import org.hibernate.HibernateException;
//import org.hibernate.Metamodel;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.cfg.Configuration;
//
//import javax.persistence.Query;
//import javax.persistence.TypedQuery;
//import javax.persistence.criteria.CriteriaBuilder;
//import javax.persistence.criteria.CriteriaQuery;
//import javax.persistence.criteria.Root;
//import javax.persistence.metamodel.EntityType;
//import java.util.List;
//
//
//public class Main6 {
//    private static final SessionFactory ourSessionFactory;
//    static {
//        try {
//            Configuration configuration = new Configuration();
//            configuration.configure();
//            ourSessionFactory = configuration.buildSessionFactory();
//        } catch (Throwable ex) {
//            throw new ExceptionInInitializerError(ex);
//        }
//    }
//    public static Session getSession() throws HibernateException {
//        return ourSessionFactory.openSession();
//    }
//    public static void main(final String[] args) {
//        final Session session = getSession();
//
//        List deps;
//        CriteriaBuilder cb = session.getCriteriaBuilder();
//        CriteriaQuery<DepartamentosEntity> cq = cb.createQuery(DepartamentosEntity.class);
//        Root<DepartamentosEntity> rootEntry = cq.from(DepartamentosEntity.class);
//        CriteriaQuery<DepartamentosEntity> all = cq.select(rootEntry);
//
//        TypedQuery<DepartamentosEntity> allQuery = session.createQuery(all);
//        deps = allQuery.getResultList();
//
//        for (Object o : deps) {
//            System.out.println("  " + o);
//        }
//
//        System.out.println();
//
//        cq.select(rootEntry).where(
//                cb.equal(rootEntry.get("loc"), "SEVILLA")
//        );
//
//        TypedQuery<DepartamentosEntity> query = session.createQuery(cq);
//
//        deps = query.getResultList();
//
//        for (Object o : deps) {
//            System.out.println("  " + o);
//        }
//
//        System.out.println();
//
//        cq.select(rootEntry).where(
//                cb.equal(rootEntry.get("dnombre"), "VENTAS")
//        );
//
//        TypedQuery<DepartamentosEntity> query2 = session.createQuery(cq);
//
//        DepartamentosEntity dep2 = query2.getSingleResult();
//
//        System.out.println(dep2.getDnombre());
//
//        DepartamentosEntity dep3 = session.find(DepartamentosEntity.class,70);
//
//        System.out.println();
//
//        System.out.println(dep3.getDnombre());
//
//        System.out.println();
//
//
//        CriteriaQuery<DepartamentosEntity> cq2 = cb.createQuery(DepartamentosEntity.class);
//        Root<DepartamentosEntity> rootEntry2 = cq2.from(DepartamentosEntity.class);
//        cq2.select(rootEntry2).orderBy(cb.asc(rootEntry2.get("dnombre")));
//
//        TypedQuery<DepartamentosEntity> allQuery2 = session.createQuery(cq2);
//        deps = allQuery2.getResultList();
//
//        for (Object o : deps) {
//            System.out.println("  " + o);
//        }
//
//
//        System.out.println();
//
//        Metamodel metamodel = session.getSessionFactory().getMetamodel();
//        EntityType<DepartamentosEntity> Dep_ = metamodel.entity(DepartamentosEntity.class);
//
//        System.out.println(Dep_.getAttribute("dnombre").getName());
//
//        System.out.println();
//
//        CriteriaBuilder cb2 = session.getCriteriaBuilder();
//        CriteriaQuery<DepartamentosEntity> cq3 = cb2.createQuery(DepartamentosEntity.class);
//        Root<DepartamentosEntity> rootEntry3 = cq3.from(DepartamentosEntity.class);
//        cq3.select(rootEntry3);
//        cq3.orderBy(cb2.desc(rootEntry3.get(Dep_.getAttribute("dnombre").getName())));
//
//        TypedQuery<DepartamentosEntity> allQuery3 = session.createQuery(cq3);
//        deps = allQuery3.getResultList();
//
//        for (Object o : deps) {
//            System.out.println("  " + o);
//        }
//
//        System.out.println();
//
//        CriteriaBuilder cb4 = session.getCriteriaBuilder();
//        CriteriaQuery<EmpleadosEntity> cq4 = cb4.createQuery(EmpleadosEntity.class);
//        Root<EmpleadosEntity> rootEntry4 = cq4.from(EmpleadosEntity.class);
//        cq4.groupBy(rootEntry4.get("departamentosByDeptNo").get("deptNo"));
//
//       TypedQuery<EmpleadosEntity> allQuery4 = session.createQuery(cq4);
//        List emps = allQuery4.getResultList();
//
//        for (Object o : emps) {
//            System.out.println("  " + o);
//        }
//
//        session.close();
//        System.exit(0);
//    }
//}
