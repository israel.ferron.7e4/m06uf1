package UF2.EA5.Menu;

import UF2.EA5.Metodes.Client_CRUD;
import UF2.EA5.Metodes.Comanda_CRUD;
import UF2.EA5.Metodes.Emp_CRUD;

import java.util.Scanner;

public class MenuEA5 extends Menu {
    Comanda_CRUD comanda_crud;
    Client_CRUD client_crud;
    Emp_CRUD emp_crud;

    public MenuEA5(Scanner scanner){
        super(scanner);
        this.comanda_crud = new Comanda_CRUD(scanner);
        this.client_crud = new Client_CRUD(scanner);
        this.emp_crud = new Emp_CRUD(scanner);
        addOption("Exercicis EA5 UF1");
        addOption("Mostra tota la informació de totes les comandes.");
        addOption("Mostra el nom del client (client_cod) i la data de comanda (com_data) de les comandes que el camp\n" +
                "com_tipus sigui null");
        addOption("Mostra el nom i el telèfon del clients de la ciutat de BURLINGAME");
        addOption("Mostra tota la informació de l’empleat amb cognom CEREZO");
        addOption("Mostra el codi (com_num) de les comandes que la data de tramesa sigui entre el 1 de gener del 1986 i el\n" +
                "30 de setembre del 1986. Utilitza el CriteriaBuilder.between.");
        addOption("Mostra el cognom i l’ofici dels empleats que cobrin més de 300.000.");
        addOption("Mostra quants empleats té cada departament. Utilitza el CriteriaQuery.groupBy");
    }

    @Override
    public void action(int select){
        switch (select){
            case 1:
                comanda_crud.selectAllComandes();
                break;
            case 2:
                comanda_crud.selectAllNullCom_Tipus();
                break;
            case 3:
                client_crud.SelectAllClientsInBURLINGAME();
                break;
            case 4:
                emp_crud.selectAllEmpsWithCEREZO();
                break;
            case 5:
                comanda_crud.selectBetweenDate();
                break;
            case 6:
                emp_crud.selectAllEmpMoreThan300000();
                break;
            case 7:
                emp_crud.selectEmpForDept();
                break;
        }
    }

}