package UF2.EA5.Metodes;

import UF2.EA5.Clases.EmpEntity;
import UF2.EA5.ConnexioEmpresa;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.util.Scanner;

public class Emp_CRUD {
    Scanner scanner;
    Session session;

    public Emp_CRUD(Scanner scanner) {
        this.scanner = scanner;
        this.session = ConnexioEmpresa.getSession();
    }

    public void selectAllEmpsWithCEREZO(){
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EmpEntity> cq = cb.createQuery(EmpEntity.class);

        Root<EmpEntity> root = cq.from(EmpEntity.class);
        CriteriaQuery<EmpEntity> query = cq.select(root).where(cb.equal(root.get("cognom"),"CEREZO"));

        TypedQuery<EmpEntity> result = session.createQuery(query);

        for (EmpEntity e : result.getResultList()){
            System.out.println(e);
        }

        session.close();
    }

    public void selectAllEmpMoreThan300000(){
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EmpEntity> cq = cb.createQuery(EmpEntity.class);

        Root<EmpEntity> root = cq.from(EmpEntity.class);
        CriteriaQuery<EmpEntity> query = cq.select(root).where(cb.greaterThan(root.get("salari"),300000));

        TypedQuery<EmpEntity> result = session.createQuery(query);

        for (EmpEntity e : result.getResultList()){
            System.out.println(e.getCognom() + " " + e.getOfici());
        }
    }

    public void selectEmpForDept(){
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);

        Root<EmpEntity> root = cq.from(EmpEntity.class);
        cq.multiselect(root.get("deptByDeptNo").get("deptNo"), cb.count(root.get("empNo")));
        cq.groupBy(root.get("deptByDeptNo").get("deptNo"));
        Query<Tuple> result = session.createQuery(cq);
        for (Tuple t : result.getResultList()){
            BigInteger id = (BigInteger) t.get(0);
            Long num = (Long) t.get(1);
            System.out.println("Dept: " + id + " Num: " + num);
        }
        session.close();


    }
}
