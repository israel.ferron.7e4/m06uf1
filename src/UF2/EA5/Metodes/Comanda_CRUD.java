package UF2.EA5.Metodes;

import UF2.EA5.Clases.ComandaEntity;
import UF2.EA5.ConnexioEmpresa;
import org.hibernate.Metamodel;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Comanda_CRUD {
    Scanner scanner;
    Session session;

    public Comanda_CRUD(Scanner scanner) {
        this.scanner = scanner;
        this.session = ConnexioEmpresa.getSession();
    }

    public void selectAllComandes(){
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ComandaEntity> cq = cb.createQuery(ComandaEntity.class);

        Root<ComandaEntity> comandaEntityRoot = cq.from(ComandaEntity.class);
        CriteriaQuery<ComandaEntity> all = cq.select(comandaEntityRoot);

        TypedQuery<ComandaEntity> allquery = session.createQuery(all);

        for (ComandaEntity c : allquery.getResultList()){
            System.out.println(c);
        }

        session.close();
    }

    public void selectAllNullCom_Tipus(){
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ComandaEntity> cq = cb.createQuery(ComandaEntity.class);

        Root<ComandaEntity> comandaEntityRoot = cq.from(ComandaEntity.class);
        CriteriaQuery<ComandaEntity> isnull = cq.select(comandaEntityRoot).where(cb.isNull(comandaEntityRoot.get("comTipus")));

        TypedQuery<ComandaEntity> query = session.createQuery(isnull);

        for (ComandaEntity c : query.getResultList()){
            System.out.println(c.getClientByClientCod().getNom() + " " + c.getComData());
        }

        session.close();
    }

    public void selectBetweenDate(){
        Date inicio = null;
        Date fin = null;
        SimpleDateFormat dateFormat = new
                SimpleDateFormat("yyyy-MM-dd");
        try {
            inicio = dateFormat.parse("1986-01-01");
            fin = dateFormat.parse("1986-07-30");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ComandaEntity> cq = cb.createQuery(ComandaEntity.class);

        Root<ComandaEntity> root = cq.from(ComandaEntity.class);
        CriteriaQuery<ComandaEntity> query = cq.select(root).where(cb.between(root.get("comData"),inicio, fin));

        TypedQuery<ComandaEntity> result = session.createQuery(query);

        for (ComandaEntity c : result.getResultList()){
            System.out.println(c.getComNum());
        }

        session.close();
    }
}
