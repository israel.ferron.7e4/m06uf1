package UF2.EA5.Metodes;

import UF2.EA5.Clases.ClientEntity;
import UF2.EA5.ConnexioEmpresa;
import org.hibernate.Metamodel;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import java.util.Scanner;

public class Client_CRUD {
    Scanner scanner;
    Session session;

    public Client_CRUD(Scanner scanner) {
        this.scanner = scanner;
        this.session = ConnexioEmpresa.getSession();
    }

    public void SelectAllClientsInBURLINGAME(){
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ClientEntity> cq = cb.createQuery(ClientEntity.class);

        Root<ClientEntity> root = cq.from(ClientEntity.class);
        CriteriaQuery<ClientEntity> query = cq.select(root).where(cb.equal(root.get("ciutat"), "BURLINGAME"));

        TypedQuery<ClientEntity> result = session.createQuery(query);

        for (ClientEntity c : result.getResultList()){
            System.out.println(c.getNom() + " " + c.getTelefon());
        }
    }
}
