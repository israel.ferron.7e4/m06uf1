package UF2.EA5;

import UF1.EA4.Menu.Color;

import UF2.EA5.Menu.MenuEA5;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuEA5 menuEA5 = new MenuEA5(scanner);
        menuEA5.showMenu();

        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!" + Color.RESET);
        }
    }
