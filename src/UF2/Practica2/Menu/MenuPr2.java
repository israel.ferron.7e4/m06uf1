package Menu;



import Clases.EstadisticasEntity;
import Metodes.Equipos_CRUD;
import Metodes.Estadisticas_CRUD;
import Metodes.Jugadors_CRUD;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuPr2 extends Menu {
    static Jugadors_CRUD jugadors_crud;
    static Estadisticas_CRUD estadisticas_crud;
    static Equipos_CRUD equipos_crud;

    public MenuPr2(Scanner scanner){
        super(scanner);
        jugadors_crud = new Jugadors_CRUD(scanner);
        estadisticas_crud = new Estadisticas_CRUD(scanner);
        equipos_crud = new Equipos_CRUD(scanner);
        addOption("Practica 2");
        addOption("Crea un mètode per insertar estadístiques per el jugador 123.");
        addOption("Crea un mètode per actualitzar el pes del jugadors. Pasarem per paràmetres el codi del jugador i el nou pes.");
        addOption("Crea un mètode que admeti un paràmetre. Aquest paràmetre és el codi del jugador, i el mètode ha de mostrar les\n" +
                "estadístiques del jugador.");
        addOption("Crea un mètode que mostri per cada equip la llista dels seus jugadors amb la mitja del punts per partit. El llistat ha\n" +
                "d’apareixer ordenat per equip i ha de mostrar també el número d’equips que hi ha.");
        addOption("Crea un mètode que mostir el jugador amb millor estadísitca de punts per cada temporada.");

    }

    @Override
    public void action(int select) throws IOException, ClassNotFoundException {
        switch (select){
            case 1:
                ex1(123);
                break;
            case 2:
                ex2();
                break;
            case 3:
                ex3();
                break;
            case 4:
                ex4();
                break;
            case 5:
                ex5();
                break;
        }
    }

    public static void ex1(int id){
        EstadisticasEntity estadistica = new EstadisticasEntity();
        estadistica.setTemporada("05/06");
        estadistica.setPuntosPorPartido((double) 7);
        estadistica.setRebotesPorPartido((double) 5);
        estadistica.setJugadoresByJugador(jugadors_crud.getJugador(id));
        estadistica.setAsistenciasPorPartido((double) 0);
        estadistica.setTaponesPorPartido((double) 0);

        EstadisticasEntity estadistica2 = new EstadisticasEntity();
        estadistica2.setTemporada("06/07");
        estadistica2.setPuntosPorPartido((double) 10);
        estadistica2.setRebotesPorPartido((double) 3);
        estadistica2.setJugadoresByJugador(jugadors_crud.getJugador(id));
        estadistica2.setAsistenciasPorPartido((double) 0);
        estadistica2.setTaponesPorPartido((double) 0);

        estadisticas_crud.insert(estadistica);
        estadisticas_crud.insert(estadistica2);
    }

    public static void ex2(){
        jugadors_crud.updateJugadorCriteria(101,240);
        jugadors_crud.updateJugadorHQL(251,200);
        jugadors_crud.updateJugadorCriteria(353,300);
        jugadors_crud.updateJugadorHQL(561,290);
        jugadors_crud.updateJugadorCriteria(407,263);
    }

    public static void ex3(){
        jugadors_crud.getEstadisticasHQL(227);
        jugadors_crud.getEstadisticasCriteria(43);
        jugadors_crud.getEstadisticasHQL(469);
        jugadors_crud.getEstadisticasHQL(4169);
    }

    public static void ex4(){
        equipos_crud.getAllPlayerOfTeamCriteria();
    }

    public static void ex5(){
        estadisticas_crud.getBestPointOfEachTempCriteria();
    }

}