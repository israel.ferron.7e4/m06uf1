package UF2.Practica2;

import Menu.Color;
import Menu.MenuPr2;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuPr2 menuPr1 = new MenuPr2(scanner);
        menuPr1.showMenu();

        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!!" + Color.RESET);
    }
}
