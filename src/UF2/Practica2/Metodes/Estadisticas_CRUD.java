package Metodes;

import Clases.EstadisticasEntity;
import Clases.JugadoresEntity;
import Menu.Color;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Scanner;

public class Estadisticas_CRUD {
    Scanner scanner;

    public Estadisticas_CRUD(Scanner scanner) {
        this.scanner = scanner;
    }

    public void insert(EstadisticasEntity est) {
        Session session = ConnectSession.getSession();
        final Transaction tx = session.beginTransaction();
        try {
            session.save(est);
            try {
                tx.commit();
            } catch (ConstraintViolationException e) {
                System.out.println(Color.RED + "JUGADOR DUPLICAT");
            } catch (Exception e) {
                System.out.println(Color.RED + "ALTRES ERRORS");
            }
        } finally {
            System.out.println(Color.ITALIC + Color.PURPLE + "Dades Insertades Correctament\n" + Color.RESET);
            session.close();
        }
    }

    public void getBestPointOfEachTempCriteria(){
        Session session = ConnectSession.getSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<String> cq = cb.createQuery(String.class);

        Root<EstadisticasEntity> root = cq.from(EstadisticasEntity.class);
        cq.select(root.get("temporada")).distinct(true);
        cq.orderBy(cb.asc(root.get("temporada")));

        Query<String> result = session.createQuery(cq);
        System.out.println(Color.PURPLE + "Millors puntuadors per temporada");
        System.out.println("=============================================");
        for (String e : result.getResultList()){
            System.out.println("Temporada: " + e);
            CriteriaQuery<Tuple> info = cb.createQuery(Tuple.class);
            CriteriaQuery<Double> maxPoints = cb.createQuery(Double.class);
            Root<EstadisticasEntity> rootInfo = info.from(EstadisticasEntity.class);
            Root<EstadisticasEntity> rootMax = maxPoints.from(EstadisticasEntity.class);

            maxPoints.select(cb.max(rootMax.get("puntosPorPartido")));
            maxPoints.where(cb.equal(rootMax.get("temporada"), e));

            Query<Double> resultMax = session.createQuery(maxPoints);
            Double max = resultMax.uniqueResult();
            System.out.println(max);

            info.multiselect(rootInfo.get("jugadoresByJugador").get("nombre"),
                    rootInfo.get("jugadoresByJugador").get("codigo"),
                    rootInfo.get("jugadoresByJugador").get("equiposByNombreEquipo").get("nombre"),
                    rootInfo.get("puntosPorPartido"));
            info.where(cb.equal(rootInfo.get("temporada"),e));
            info.groupBy(rootInfo.get("jugadoresByJugador").get("nombre"),
                    rootInfo.get("jugadoresByJugador").get("codigo"),
                    rootInfo.get("jugadoresByJugador").get("equiposByNombreEquipo").get("nombre"),
                    rootInfo.get("puntosPorPartido"));
            info.having(cb.equal(rootInfo.get("puntosPorPartido"), max));

            Query<Tuple> inforList = session.createQuery(info);
            Tuple t = inforList.uniqueResult();
            System.out.print(t.get(1) + " ");
            System.out.print(t.get(0) + " ");
            System.out.println(t.get(2));
            System.out.println("Punts: " + t.get(3));
            System.out.println("===================================================");
        }
    }
}
