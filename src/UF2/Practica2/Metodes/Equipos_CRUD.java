package Metodes;

import Clases.EquiposEntity;
import Clases.EstadisticasEntity;
import Clases.JugadoresEntity;
import Menu.Color;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Scanner;

public class Equipos_CRUD {
    Scanner scanner;

    public Equipos_CRUD(Scanner scanner) {
        this.scanner = scanner;
    }

    public void getAllPlayerOfTeamCriteria(){
        Session session = ConnectSession.getSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EquiposEntity> query = cb.createQuery(EquiposEntity.class);

        Root<EquiposEntity> root = query.from(EquiposEntity.class);
        query.select(root);
        query.orderBy(cb.asc(root.get("nombre")));
        Query<EquiposEntity> result = session.createQuery(query);
        System.out.println(Color.PURPLE + "Numero d'Equips: " + result.getResultList().size());
        System.out.println("==============================================");


        for (EquiposEntity e : result.getResultList()){
            System.out.println("Equip: " + e.getNombre());
            for (JugadoresEntity j : e.getJugadoresByNombre()){
                System.out.print(j.getCodigo() + "," + j.getNombre() + " ");

                CriteriaQuery<Double> queryavg = cb.createQuery(Double.class);
                Root<EstadisticasEntity> rootavg = queryavg.from(EstadisticasEntity.class);

                queryavg.select(cb.avg(rootavg.get("puntosPorPartido")));
                queryavg.where(cb.equal(rootavg.get("jugadoresByJugador"),j));
                queryavg.groupBy(rootavg.get("jugadoresByJugador").get("nombre"),rootavg.get("jugadoresByJugador").get("codigo"));

                Double avg;
                try {
                    avg = session.createQuery(queryavg).getSingleResult();
                }catch (NoResultException es){
                    avg = 0.00;
                }
                System.out.println(String.valueOf(Math.round(avg*100.0)/100.0));
            }
            System.out.println("==================================================");
        }


    }

    public void getAllPlayerOfTeamHQL(){
        Session session = ConnectSession.getSession();
        Query query1 = session.createQuery("SELECT EquiposEntity from EquiposEntity ");
        List<EquiposEntity> listEquip = query1.getResultList();

        System.out.println(Color.PURPLE + "Numero d'Equips: " + listEquip.size());
        System.out.println("==============================================");
        for (EquiposEntity e : listEquip){
            System.out.println("Equip: " + e.getNombre());
            Query query2 = session.createQuery(
                    "SELECT j.nombre, j.codigo, avg(est.puntosPorPartido) FROM JugadoresEntity j, EquiposEntity e, EstadisticasEntity est WHERE j.codigo = est.jugadoresByJugador.codigo AND j.equiposByNombreEquipo.nombre = :id");
            query2.setParameter("id", e.getNombre());

            List<Tuple> listInfo = query2.getResultList();
            for (Tuple t : listInfo){
                System.out.print(t.get(1)+ "," + t.get(0)+ " " + t.get(2));
            }
        }
        System.out.println("==================================================");
        session.close();
    }
}
