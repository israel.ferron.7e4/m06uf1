package Metodes;

import Clases.EstadisticasEntity;
import Clases.JugadoresEntity;
import Menu.Color;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Scanner;

public class Jugadors_CRUD {
    Scanner scanner;

    public Jugadors_CRUD(Scanner scanner) {
        this.scanner = scanner;
    }

    public void updateJugadorHQL(int id, int peso){
        Session session = ConnectSession.getSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("UPDATE JugadoresEntity SET peso = :valor WHERE id = :id");
        query.setParameter("valor", peso);
        query.setParameter("id", id);
        query.executeUpdate();
        tx.commit();
        session.close();
    }

    public JugadoresEntity getJugador(int id){
        Session session = ConnectSession.getSession();
        Query query = session.createQuery("SELECT e FROM JugadoresEntity e where codigo = :id");
        query.setParameter("id", id);

        Object result = query.getSingleResult();
        JugadoresEntity jugador = (JugadoresEntity) result;
        return jugador;
    }

    public void  updateJugadorCriteria(int id, int peso){
        Session session = ConnectSession.getSession();
        Transaction tx = session.beginTransaction();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaUpdate<JugadoresEntity> cu = cb.createCriteriaUpdate(JugadoresEntity.class);
        Root<JugadoresEntity> root = cu.from(JugadoresEntity.class);
        cu.set("peso", peso);
        cu.where(cb.equal(root.get("codigo"),id));
        session.createQuery(cu).executeUpdate();
        tx.commit();
        session.close();
    }

    public void getEstadisticasHQL(int id){
        try {
            Session session = ConnectSession.getSession();
            Query query = session.createQuery("SELECT j from JugadoresEntity j where j.codigo = :id");
            query.setParameter("id", id);

            Object result = query.getSingleResult();
            JugadoresEntity jugador = (JugadoresEntity) result;

            System.out.println(Color.PURPLE + "DADES DEL JUGADOR: " + jugador.getCodigo());
            System.out.println("NOM: " + jugador.getNombre());
            System.out.println("Equipo: " + jugador.getEquiposByNombreEquipo().getNombre());
            System.out.println("Temporada    Punts     Assitencies    Taps    Rebots");
            System.out.println("=======================================================");
            for (EstadisticasEntity est : jugador.getEstadisticasByCodigo()) {
                System.out.println(est.getTemporada() + "        " + est.getPuntosPorPartido() + "      " + est.getAsistenciasPorPartido() + "            " + est.getTaponesPorPartido() + "     " + est.getRebotesPorPartido());
            }
            System.out.println("=======================================================");
            System.out.println("Num de registres: " + jugador.getEstadisticasByCodigo().size());
            System.out.println("=======================================================");
            System.out.println("=======================================================" + Color.RESET);
            session.close();
        }catch (NoResultException e){
            System.out.println("No existeix aquest el jugador amb id " + id);
        }
    }

    public void getEstadisticasCriteria(int id){
        try {
            Session session = ConnectSession.getSession();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<JugadoresEntity> cq = cb.createQuery(JugadoresEntity.class);

            Root<JugadoresEntity> root = cq.from(JugadoresEntity.class);
            CriteriaQuery<JugadoresEntity> query =
                    cq.select(root).where(cb.equal(root.get("codigo"),id));

            JugadoresEntity jugador = session.createQuery(query).getSingleResult();

            System.out.println(Color.PURPLE + "DADES DEL JUGADOR: " + jugador.getCodigo());
            System.out.println("NOM: " + jugador.getNombre());
            System.out.println("Equipo: " + jugador.getEquiposByNombreEquipo().getNombre());
            System.out.println("Temporada    Punts     Assitencies    Taps    Rebots");
            System.out.println("=======================================================");
            for (EstadisticasEntity est : jugador.getEstadisticasByCodigo()){
                System.out.println(est.getTemporada() + "        " + est.getPuntosPorPartido() + "      " + est.getAsistenciasPorPartido() + "            " + est.getTaponesPorPartido() + "     " + est.getRebotesPorPartido());
            }
            System.out.println("=======================================================");
            System.out.println("Num de registres: " + jugador.getEstadisticasByCodigo().size());
            System.out.println("=======================================================");
            System.out.println("=======================================================" + Color.RESET);
            session.close();
        }catch (NoResultException e){
            System.out.println("No existeix aquest el jugador amb id " + id);
        }


    }
}
