package UF1.EA6;

import UF1.EA1.Menu.Color;
import UF1.EA6.Menu.MenuEA6;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuEA6 menuEA6 = new MenuEA6(scanner);
        menuEA6.showMenu();

        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!" + Color.RESET);
    }
}