package UF1.EA6.Metodes;

import UF1.EA4.Classes.Departament;
import UF1.EA4.Classes.Empleat;
import UF1.EA4.Menu.Color;
import UF1.EA6.Clases.*;
import UF1.Practica1.Classes.Producte;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import java.io.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class MetodesEA6 {
    Scanner scanner;

    public MetodesEA6(Scanner scanner) {
        this.scanner = scanner;
    }

    public void Ex1() throws IOException, ClassNotFoundException {
        Persona persona; // defino la variable persona
        File fichero = new File("FichPersona.dat");
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(fichero));
        ListaPersonas listaper = new ListaPersonas();
        try {
            while (true) { // lectura del fichero
                persona = (Persona) dataIS.readObject(); // leer una UF1.Persona
                listaper.add(persona);
            }
        } catch (EOFException eo) {
            System.out.println("FIN DE LECTURA.");
        } catch (StreamCorruptedException x) {
        }
        dataIS.close();// cerrar stream de entrada
        try {
            XStream xstream = new XStream();
            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaPersonasMunicipio",
                    ListaPersonas.class);
            xstream.alias("DatosPersona", Persona.class);

            //xstream.aliasField("Nombre alumno", UF1.Persona.class, "nombre");
            //xstream.aliasField("Edad alumno", UF1.Persona.class, "edad");

            //quitar etiqueta lista (atributo de la claseListaPersonas)
            xstream.addImplicitCollection
                    (ListaPersonas.class, "list");
            //Insertar los objetos en el XML
            xstream.toXML(listaper, new
                    FileOutputStream("Personas.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e)
        {e.printStackTrace();}
    }

    public void Ex2() throws IOException{
        XStream xstream = new XStream();

        xstream.alias("ListaPersonasMunicipio", ListaPersonas.class);
        xstream.alias("DatosPersona", Persona.class);
        xstream.addImplicitCollection(ListaPersonas.class, "list");

        ListaPersonas listadoTodas = (ListaPersonas)
                xstream.fromXML(new FileInputStream("Personas.xml"));
        System.out.println("Numero de Personas: " +
                listadoTodas.getListaPersonas().size());

        List<Persona> listaPersonas = new ArrayList<Persona>();
        listaPersonas = listadoTodas.getListaPersonas();

        Iterator iterador = listaPersonas.listIterator();
        while( iterador.hasNext() ) {
            Persona p = (Persona) iterador.next();
            System.out.printf("Nombre: %s, edad: %d %n",
                    p.getNombre(), p.getEdad());
        }
        System.out.println("Fin de listado .....");
    }

    public void Ex3() throws IOException, ClassNotFoundException {
        Empleat empleat; // defino la variable persona
        File fichero = new File("FitxerEmpleats.dat");
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(fichero));
        ListaEmpleats listemp = new ListaEmpleats();
        try {
            while (true) { // lectura del fichero
                empleat = (Empleat) dataIS.readObject(); // leer una UF1.Persona
                listemp.add(empleat);
            }
        } catch (EOFException eo) {
            System.out.println("FIN DE LECTURA.");
        } catch (StreamCorruptedException x) {
        }
        dataIS.close();// cerrar stream de entrada
        try {
            XStream xstream = new XStream();
            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaEmpresa",
                    ListaEmpleats.class);
            xstream.alias("DatosEmpleado", Empleat.class);

            //xstream.aliasField("Nombre alumno", UF1.Persona.class, "nombre");
            //xstream.aliasField("Edad alumno", UF1.Persona.class, "edad");

            //quitar etiqueta lista (atributo de la claseListaPersonas)
            xstream.addImplicitCollection
                    (ListaEmpleats.class, "list");
            //Insertar los objetos en el XML
            xstream.toXML(listemp, new
                    FileOutputStream("Empleats.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e)
        {e.printStackTrace();}
    }

    public void Ex4() throws IOException, ClassNotFoundException {
        Departament departament; // defino la variable persona
        File fichero = new File("FitxerDepartaments.dat");
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(fichero));
        ListaDepartaments listdep = new ListaDepartaments();
        try {
            while (true) { // lectura del fichero
                departament = (Departament) dataIS.readObject(); // leer una UF1.Persona
                listdep.add(departament);
            }
        } catch (EOFException eo) {
            System.out.println("FIN DE LECTURA.");
        } catch (StreamCorruptedException x) {
        }
        dataIS.close();// cerrar stream de entrada
        try {
            XStream xstream = new XStream();
            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaEmpresa",
                    ListaDepartaments.class);
            xstream.alias("DatosDepartamento", Departament.class);

            //xstream.aliasField("Nombre alumno", UF1.Persona.class, "nombre");
            //xstream.aliasField("Edad alumno", UF1.Persona.class, "edad");

            //quitar etiqueta lista (atributo de la claseListaPersonas)
            xstream.addImplicitCollection
                    (ListaDepartaments.class, "list");
            //Insertar los objetos en el XML
            xstream.toXML(listdep, new
                    FileOutputStream("Departaments.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e)
        {e.printStackTrace();}
    }

    public void Ex5() throws IOException{
        XStream xstream = new XStream();

        xstream.alias("ListaEmpresa", ListaEmpleats.class);
        xstream.alias("DatosEmpleado", Empleat.class);
        xstream.addImplicitCollection(ListaEmpleats.class, "list");

        ListaEmpleats listadoTodas = (ListaEmpleats)
                xstream.fromXML(new FileInputStream("Empleats.xml"));
        System.out.println("Numero de Personas: " +
                listadoTodas.getListaPersonas().size());

        List<Empleat> listaPersonas = new ArrayList<Empleat>();
        listaPersonas = listadoTodas.getListaPersonas();

        Iterator iterador = listaPersonas.listIterator();
        while( iterador.hasNext() ) {
            Empleat p = (Empleat) iterador.next();
            System.out.println("ID: " + p.getId() + ", Cognom: " + p.getCognom() + ", Dep: " + p.getDep() + ", SSalari: " + p.getSalari());
        }
        System.out.println("Fin de listado .....");
    }

    public void Ex6() throws IOException{
        XStream xstream = new XStream();

        xstream.alias("ListaEmpresa", ListaDepartaments.class);
        xstream.alias("DatosDepartamento", Departament.class);
        xstream.addImplicitCollection(ListaDepartaments.class, "list");

        ListaDepartaments listadoTodas = (ListaDepartaments)
                xstream.fromXML(new FileInputStream("Departaments.xml"));
        System.out.println("Numero de Personas: " +
                listadoTodas.getListaPersonas().size());

        List<Departament> listaPersonas = new ArrayList<>();
        listaPersonas = listadoTodas.getListaPersonas();

        Iterator iterador = listaPersonas.listIterator();
        while( iterador.hasNext() ) {
            Departament p = (Departament) iterador.next();
            System.out.println("ID: " + p.getId() + ", Nom: " + p.getNom() + ", Localitat: " + p.getLocalitat());
        }
        System.out.println("Fin de listado .....");
    }

//    public void Ex7() throws IOException{
//        Gson gson = new Gson();
//        String file = "";
//        try (BufferedReader br = new BufferedReader(new FileReader("products1.json"))){
//            String linea;
//            while ((linea = br.readLine()) != null){
//                file += linea;
//            }
//        }catch (FileNotFoundException ex){
//            System.out.println(ex.getMessage());
//        }catch (IOException ex){
//            System.out.println(ex.getMessage());
//        }
//        Producte[] productes =  gson.fromJson(file, Producte[].class);
//
//
//        File fich = new File("./Products1.dat");
//        if (!fich.exists()) {
//            Producte producte;
//            FileOutputStream fileOut = new FileOutputStream(fich, true);
//            ObjectOutputStream dataOS = new ObjectOutputStream(fileOut);
//
//            for (Producte p : productes){
//                producte = new Producte(p.getName(),p.getPrice(), p.getStock(), p.getPicture(),p.getCategorias());
//                dataOS.writeObject(producte);
//            }
//            System.out.println("S'ha introduit les dades al fitxer Products correctament");
//            dataOS.close();
//        }else
//            System.out.println(Color.RED + Color.ITALIC + "Error algun dels fitxers Products ja existeix"+ Color.RESET);
//    }

    public void Ex8() throws IOException, ClassNotFoundException {
        Producte producte; // defino la variable persona
        File fichero = new File("Products1.dat");
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(fichero));
        ListaProductes listpr = new ListaProductes();
        try {
            while (true) { // lectura del fichero
                producte = (Producte) dataIS.readObject(); // leer una UF1.Persona
                listpr.add(producte);
            }
        } catch (EOFException eo) {
            System.out.println("FIN DE LECTURA.");
        } catch (StreamCorruptedException x) {
        }
        dataIS.close();// cerrar stream de entrada
        try {
            XStream xstream = new XStream();
            //cambiar de nombre a las etiquetas XML
            xstream.alias("ListaMercadona",
                    ListaProductes.class);
            xstream.alias("DatosProducto", Producte.class);

            //xstream.aliasField("Nombre alumno", UF1.Persona.class, "nombre");
            //xstream.aliasField("Edad alumno", UF1.Persona.class, "edad");

            //quitar etiqueta lista (atributo de la claseListaPersonas)
            xstream.addImplicitCollection
                    (ListaProductes.class, "list");
            //Insertar los objetos en el XML
            xstream.toXML(listpr, new
                    FileOutputStream("Products1.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e)
        {e.printStackTrace();}
    }

}