package UF1.EA6.Menu;

import UF1.EA6.Metodes.MetodesEA6;

import java.io.IOException;
import java.util.Scanner;

public class MenuEA6 extends Menu {
    MetodesEA6 metodesEA6;

    public MenuEA6(Scanner scanner) {
        super(scanner);
        metodesEA6 = new MetodesEA6(scanner);
        addOption("Exercicis UF1.EA6 UF1");
        addOption("Crea els mètodes per crear el fitxer Personas.xml");
        addOption("Crea els mètodes per llegir el fitxer Personas.xml");
        addOption("Crear el fitxers Empleats.xml");
        addOption("Crear el fitxers Departaments.xml");
        addOption("Llegir els fitxers Empleats.xml");
        addOption("Llegir els fitxers Departaments.xml");
        addOption("Llegeix el fitxer products1.json i amb els\n" +
                "objectes Product1 obtinguts crea un fitxer binari anomenat Products1.dat.");
        addOption("Crear el fitxer Products1.xml a partir dels objectes\n" +
                "emmagatzemats en el fitxer binari Products1.dat.");
    }

    @Override
    public void action(int select) throws IOException, ClassNotFoundException {
        switch (select) {
            case 1:
                metodesEA6.Ex1();
                break;
            case 2:
                metodesEA6.Ex2();
                break;
            case 3:
                metodesEA6.Ex3();
                break;
            case 4:
                metodesEA6.Ex4();
                break;
            case 5:
                metodesEA6.Ex5();
                break;
            case 6:
                metodesEA6.Ex6();
                break;
            case 7:
//                metodesEA6.Ex7();
                break;
            case 8:
                metodesEA6.Ex8();
                break;
        }
    }
}