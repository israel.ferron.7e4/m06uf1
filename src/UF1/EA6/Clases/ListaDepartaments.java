package UF1.EA6.Clases;

import UF1.EA4.Classes.Departament;

import java.util.ArrayList;
import java.util.List;

public class ListaDepartaments {
    private List<Departament> list = new ArrayList<>();

    public ListaDepartaments() {
    }

    public void add(Departament per){
        list.add(per);
    }

    public List<Departament> getList() {
        return list;
    }

    public void setList(List<Departament> list) {
        this.list = list;
    }

    public List<Departament> getListaPersonas() {
        return list;
    }
}