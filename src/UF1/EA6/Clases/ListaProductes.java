package UF1.EA6.Clases;

import UF1.Practica1.Classes.Producte;

import java.util.ArrayList;
import java.util.List;

public class ListaProductes {
    private List<Producte> list = new ArrayList<>();

    public ListaProductes() {
    }

    public void add(Producte per){
        list.add(per);
    }

    public List<Producte> getList() {
        return list;
    }

    public void setList(List<Producte> list) {
        this.list = list;
    }

    public List<Producte> getListaPersonas() {
        return list;
    }
}
