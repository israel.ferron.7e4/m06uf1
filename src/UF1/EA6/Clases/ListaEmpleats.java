package UF1.EA6.Clases;

import UF1.EA4.Classes.Empleat;

import java.util.ArrayList;
import java.util.List;

public class ListaEmpleats {
    private List<Empleat> list = new ArrayList<>();

    public ListaEmpleats() {
    }

    public void add(Empleat per){
        list.add(per);
    }

    public List<Empleat> getList() {
        return list;
    }

    public void setList(List<Empleat> list) {
        this.list = list;
    }

    public List<Empleat> getListaPersonas() {
        return list;
    }
}