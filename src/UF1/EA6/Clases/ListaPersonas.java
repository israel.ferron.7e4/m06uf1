package UF1.EA6.Clases;

import java.util.ArrayList;
import java.util.List;

public class ListaPersonas {
    private List<Persona> list = new ArrayList<>();

    public ListaPersonas() {
    }

    public void add(Persona per){
        list.add(per);
    }

    public List<Persona> getList() {
        return list;
    }

    public void setList(List<Persona> list) {
        this.list = list;
    }

    public List<Persona> getListaPersonas() {
        return list;
    }
}
