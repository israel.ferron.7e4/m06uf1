package UF1.EA3;

import UF1.EA3.Menu.Color;
import UF1.EA3.Menu.MenuEA3;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuEA3 menuEA1 = new MenuEA3(scanner);
        menuEA1.showMenu();

        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!" + Color.RESET);

    }
}
