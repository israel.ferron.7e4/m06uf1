package UF1.EA3.Metodes;

import UF1.EA3.Menu.Color;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MetodesEA3 {
    Scanner scanner;

    //Contructor
    public MetodesEA3(Scanner scanner) {
        this.scanner = scanner;
    }

    //Exercici1
    public void Ex1() throws IOException {
        File file = new File("Departaments.dat");
        if (!file.exists()) {
            DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(file));

            //Creem les arrays amb els valors
            int[] ids = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            String[] deps = {"Marketing", "I+D", "R+D", "Recursos Humanos", "Financias",
                    "Compras", "Ventas", "Logistica", "Control de Gestion", "Laboriatorio"};
            String[] ciutats = {"Monaco", "Puerto de España", "Phnom Penh", "Nairobi", "Bixkek",
                    "Lomé", "Ulan Bator", "Rabat", "Santo Domingo", "Riga"};

            //Introduim els valor amb un for
            for (int i = 0; i < ids.length; i++) {
                dataOS.writeInt(ids[i]);
                dataOS.writeUTF(deps[i]);
                dataOS.writeUTF(ciutats[i]);
            }
            System.out.println("Fitxer creat correctament");
            dataOS.close();
        }else
            System.out.println("El fitxer ja existeix");
    }

    //Exercici2
    public void Ex2()throws IOException{
        File file = new File("Departaments.dat");
        if (file.exists()) {
            DataInputStream dataIs = new DataInputStream(new FileInputStream(file));

            //Pedim l'imformació a cambiar
            System.out.print("Posa la id del departament a modificar: ");
            scanner.nextLine();
            int id_c = scanner.nextInt();
            System.out.print("Quin nom li vols posar al departament: ");
            scanner.nextLine();
            String nom_c = scanner.nextLine();
            System.out.print("En quina ciutat està: ");
            String ciutat_c = scanner.nextLine();
            List<Integer> ids = new ArrayList<>();
            List<String> noms = new ArrayList<>();
            List<String> ciutats = new ArrayList<>();


            try {
                while (true) {
                    int id = dataIs.readInt();
                    String nom = dataIs.readUTF();
                    String ciutat = dataIs.readUTF();
                    if (id == id_c) {
                        System.out.println(Color.PURPLE + "Departamets que has cambiat: "+ Color.RESET);
                        System.out.println(Color.CYAN + Color.BOLD + "ID: " + id + " DEPARTAMENTS: " +
                                nom + " CIUTAT :" + ciutat);
                        System.out.println(Color.PURPLE + "Departamets a cambiades: "+ Color.RESET);
                        System.out.println(Color.CYAN + Color.BOLD + "ID: " + id_c + " DEPARTAMENTS: " +
                                nom_c + " CIUTAT :" + ciutat_c + Color.RESET);

                    }
                    ids.add(id);
                    noms.add(nom);
                    ciutats.add(ciutat);
                }
            } catch (EOFException e) {
            }

            file.delete();
            DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(file));
            for (int i = 0; i < ids.size(); i++) {
                dataOS.writeInt(ids.get(i));
                if (ids.get(i) == id_c){
                    dataOS.writeUTF(nom_c);
                    dataOS.writeUTF(ciutat_c);
                    continue;
                }
                dataOS.writeUTF(noms.get(i));
                dataOS.writeUTF(ciutats.get(i));
            }
            System.out.println("Dades cambiades correctament");
            dataIs.close();
            dataOS.close();
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error! El fitxer no exiteix"+ Color.RESET);
    }

    //Exercici3
    public void Ex3 () throws IOException{
        File file = new File("Departaments.dat");
        if (file.exists()) {
            DataInputStream dataIs = new DataInputStream(new FileInputStream(file));

            //Pedim l'imformació a cambiar
            System.out.print("Posa la id del departament a modificar: ");
            scanner.nextLine();
            int id_e = scanner.nextInt();
            List<Integer> ids = new ArrayList<>();
            List<String> noms = new ArrayList<>();
            List<String> ciutats = new ArrayList<>();


            int id;
            String dep;
            String ciutat;
            int ndeps = 0;

            try {
                while (true) {
                    id = dataIs.readInt();
                    dep = dataIs.readUTF();
                    ciutat = dataIs.readUTF();
                    if (id == id_e) {
                        continue;
                    }
                    ids.add(id);
                    noms.add(dep);
                    ciutats.add(ciutat);
                    ndeps++;
                }

            } catch (EOFException e) {
            }
            System.out.println(Color.RED + Color.ITALIC + "Hi hi en total: " + ndeps + " depataments");

            file.delete();
            DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(file));
            for (int i = 0; i < ids.size(); i++) {
                dataOS.writeInt(ids.get(i));
                dataOS.writeUTF(noms.get(i));
                dataOS.writeUTF(ciutats.get(i));
            }
            System.out.println("Dades eliminades correctament");
            dataIs.close();
            dataOS.close();
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error! El fitxer no exiteix"+ Color.RESET);
    }

    //Exercici4
    public void Ex4() throws IOException {
        File file = new File("Departaments.dat");
        if (file.exists()) {
            DataInputStream dataIs = new DataInputStream(new FileInputStream(file));

            //Crear les variables
            int id;
            String dep;
            String ciutat;

            //Llegir el fitxer amb un for
            try {
                while (true) {
                    id = dataIs.readInt();
                    dep = dataIs.readUTF();
                    ciutat = dataIs.readUTF();
                    System.out.println(Color.CYAN + Color.BOLD + "ID: " + id + " DEPARTAMENTS: " +
                            dep + " CIUTAT: " + ciutat);
                    System.out.println(Color.PURPLE + "------------------------------" + Color.RESET);
                }
            } catch (EOFException e) {
            }
            dataIs.close();
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error! El fitxer no exiteix"+ Color.RESET);
    }
}
