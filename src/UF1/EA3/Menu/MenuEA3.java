package UF1.EA3.Menu;

import UF1.EA3.Metodes.MetodesEA3;

import java.io.IOException;
import java.util.Scanner;

public class MenuEA3 extends Menu {
    MetodesEA3 metodesEA3;

    public MenuEA3(Scanner scanner){
        super(scanner);
        metodesEA3 = new MetodesEA3(scanner);
        addOption("Exercicis UF1.EA1 UF1");
        addOption("1-Crear un fitxer binari per guardar dades de departaments, el nom del fitxer és \"Departaments.dat\".");
        addOption("2-Modificar les dades d'un departament.");
        addOption("3-Eliminar físicament un departament.");
        addOption("4-Necessitaràs també crear un mètode per llegir el fitxer binari creat.");
    }

    @Override
    public void action(int select) throws IOException {
        switch (select){
            case 1:
                metodesEA3.Ex1();
                break;
            case 2:
                metodesEA3.Ex2();
                break;
            case 3:
                metodesEA3.Ex3();
                break;
            case 4:
                metodesEA3.Ex4();
                break;
        }
    }

}
