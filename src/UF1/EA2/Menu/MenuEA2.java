package UF1.EA2.Menu;

import UF1.EA2.Metodes.MetodesEA2;


import java.io.IOException;
import java.util.Scanner;

public class MenuEA2 extends Menu {
    MetodesEA2 metodesEA2;

    public MenuEA2(Scanner scanner){
        super(scanner);
        metodesEA2 = new MetodesEA2(scanner);
        addOption("Exercicis UF1.EA1 UF1");
        addOption("Consultar les dades d'un empleat del fitxer \"AleatorioEmple.dat\".");
        addOption("Insertar dades en el fitxer \"AleatorioEmple.dat\".");
        addOption("Modificar dades en el fitxer \"AleatorioEmple.dat\".");
        addOption("Esborrar dades en el fitxer \"AleatorioEmple.dat\".");
        addOption("Crea un mètode que mostri els identificadors dels empleats eliminats.");
    }

    @Override
    public void action(int select) throws IOException {
        switch (select){
            case 1:
                metodesEA2.Ex1();
                break;
            case 2:
                metodesEA2.Ex2();
                break;
            case 3:
                metodesEA2.Ex3();
                break;
            case 4:
                metodesEA2.Ex4();
                break;
            case 5:

                break;

        }
    }

}
