package UF1.EA2.Metodes;
import UF1.EA2.Menu.Color;

import java.io.File;
import java.io.*;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class MetodesEA2 {
    Scanner scanner;

    public MetodesEA2(Scanner scanner) {
        this.scanner = scanner;
    }

    //Ex1
    public void Ex1() throws IOException {
        System.out.print(Color.ITALIC + "Quina id vols posar?: ");
        scanner.nextLine();
        int od = scanner.nextInt();

        File file = new File("AleatorioEmple.dat");
        RandomAccessFile randomFile = new RandomAccessFile(file,"r");

        int id, dep, posicio;
        Double salario;
        char apellido[] = new char[10], aux;
        int registro = od;

        posicio = (registro -1) * 36;
        randomFile.seek(posicio);
        id=randomFile.readInt();
        if (od != id){
            System.out.println("No existeix l'id");
        }else {
            for (int i = 0; i < apellido.length; i++) {
                aux = randomFile.readChar();
                apellido[i] = aux;
            }
            String apellidos = new String(apellido);
            dep = randomFile.readInt();
            salario = randomFile.readDouble();

            System.out.println("ID: " + registro + ", Apellido: " + apellidos.trim() + ", Departamento: " + dep + ", Salario: " + salario);
        }
        randomFile.close();
    }

    //Ex2
    public void Ex2() throws IOException{
        File file = new File("AleatorioEmple.dat");
        RandomAccessFile randomFile = new RandomAccessFile(file,"rw");
        System.out.println(Color.BOLD + "Creant un nou regsitre..." + Color.RESET);
        System.out.print(Color.ITALIC + "Quina id vols posar?: ");
        scanner.nextLine();
        int id = scanner.nextInt();
        System.out.print(Color.ITALIC + "Quin cognom vols posar?: ");
        String apellido = scanner.next();
        System.out.print(Color.ITALIC + "Quin salari té?: ");
        Double salario = scanner.nextDouble();
        System.out.print(Color.ITALIC + "A quin departament pertany?: " + Color.RESET);
        int dep = scanner.nextInt();

        StringBuffer buffer = null;
        long posicio = (id -1) *36;
        randomFile.seek(posicio);
        randomFile.writeInt(id);
        buffer = new StringBuffer(apellido);
        buffer.setLength(10);
        randomFile.writeChars(buffer.toString());
        randomFile.writeInt(dep);
        randomFile.writeDouble(salario);
        randomFile.close();
        System.out.println(Color.BOLD + "Registre crear correctament" + Color.RESET);
    }

    //Ex3
    public void Ex3() throws IOException{
        int ele;
        System.out.print(Color.ITALIC + "Quina id vols posar?: ");
        scanner.nextLine();
        int id = scanner.nextInt();
        System.out.print(Color.ITALIC + "Quin salari té?: ");
        Double salario = scanner.nextDouble();

        File file = new File("AleatorioEmple.dat");
        RandomAccessFile randomFile = new RandomAccessFile(file,"rw");

        long posicio = (id -1) * 36;
        int od = randomFile.readInt();
        if (od != id){
            System.out.println("No existeix l'id");
        }else {
            posicio = posicio + 4 + 20 + 4;
            randomFile.seek(posicio);
            double salarioemp = randomFile.readDouble();
            salario += salarioemp;
            randomFile.writeDouble(salario);
        }
        randomFile.close();
    }

    public void Ex4() throws IOException{
        System.out.print(Color.ITALIC + "Quina id vols posar?: ");
        scanner.nextLine();
        int od = scanner.nextInt();

        File file = new File("AleatorioEmple.dat");
        RandomAccessFile randomFile = new RandomAccessFile(file,"r");

        int id, posicio;
        Double salario;
        char apellido[] = new char[10], aux;
        int registro = od;

        posicio = (registro -1) * 36;
        randomFile.seek(posicio);
        id=randomFile.readInt();
        if (od != id){
            System.out.println("No existeix l'id");
        }else {
            randomFile.writeInt(-1);
        }
        randomFile.close();
    }
}