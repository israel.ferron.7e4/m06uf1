package UF1.EA2;

import UF1.EA2.Menu.Color;
import UF1.EA2.Menu.MenuEA2;


import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuEA2 menuEA1 = new MenuEA2(scanner);
        menuEA1.showMenu();

        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!" + Color.RESET);
    }
}
