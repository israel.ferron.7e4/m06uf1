package UF1.Practica1.Classes;

import java.io.Serializable;

public class Producte implements Serializable {
    private int codi;
    private String nom;
    private double preu;
    private String magatzem;
    private int estanteria;

    public Producte(int codi, String nom, double preu, String magatzem, int estanteria) {
        this.codi = codi;
        this.nom = nom;
        this.preu = preu;
        this.magatzem = magatzem;
        this.estanteria = estanteria;
    }

    public int getCodi() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi = codi;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPreu() {
        return preu;
    }

    public void setPreu(double preu) {
        this.preu = preu;
    }

    public String getMagatzem() {
        return magatzem;
    }

    public void setMagatzem(String magatzem) {
        this.magatzem = magatzem;
    }

    public int getEstanteria() {
        return estanteria;
    }

    public void setEstanteria(int estanteria) {
        this.estanteria = estanteria;
    }

    @Override
    public String toString() {
        return "Producte{" +
                "codi=" + codi +
                ", nom='" + nom + '\'' +
                ", preu=" + preu +
                ", magatzem='" + magatzem + '\'' +
                ", estanteria=" + estanteria +
                '}';
    }
}
