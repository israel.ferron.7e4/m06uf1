package UF1.Practica1;


import UF1.Practica1.Menu.Color;
import UF1.Practica1.Menu.MenuPr1;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuPr1 menuPr1 = new MenuPr1(scanner);
        menuPr1.showMenu();

        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!!" + Color.RESET);
    }
}
