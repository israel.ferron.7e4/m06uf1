package UF1.Practica1.Metodes;


import UF1.Practica1.Classes.Producte;
import UF1.Practica1.Menu.Color;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetodesPr1 {
    Scanner scanner;

    //Contructor
    public MetodesPr1(Scanner scanner) {
        this.scanner = scanner;
    }

    //Exercici1
    //CrearFitxer

    /**
     * Crear el fitxer Producte.dat
     * @throws IOException
     */
    public void Ex1_Create() throws IOException{
        File file = new File("./Productes.dat");
        if (!file.exists()) {
            Producte producte;
            FileOutputStream fileOut = new FileOutputStream(file, true);
            ObjectOutputStream dataOS = new ObjectOutputStream(fileOut);

            int codi[] = {1,2,3,4,5};
            String nom[] = {"Maduixes","Platans","Pastanagues","Olives","Macarrons"};
            double preu[] = {2.5,5,3.2,1,6.6};
            String magatzem[] = {"Ala Oest","Ala Sud", "Ala Nord", "Ala Est", "Centr"};
            int estanteria[] = {10,20,30,40,50};

            for (int i = 0; i < codi.length; i++) {
                producte = new Producte(codi[i],nom[i],preu[i],magatzem[i],estanteria[i]);
                dataOS.writeObject(producte);
            }
            System.out.println("S'ha introduit les dades al fitxer Productes correctament");
            dataOS.close();
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error algun dels fitxers Departaments ja existeix"+ Color.RESET);
    }

    //LlegirFitxer

    /**
     * Llegir el fitxer Producte.dat
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void Ex1_Read() throws IOException, ClassNotFoundException{
        File file = new File("./Productes.dat");
        if (file.exists()){
            ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(file));
            Producte producte;

            int i = 1;
            System.out.printf(Color.CYAN+ "Taula Productes");
            System.out.println("=========================" + Color.RESET);
            try {
                while (true){
                    producte = (Producte) dataIS.readObject();
                    System.out.print(Color.PURPLE + i + "=>");
                    i++;
                    System.out.printf("Codi: %d, Nom: %s, Preu: %.1f, Magatzem: %s, Estanteria: %d \n",producte.getCodi(),producte.getNom(),producte.getPreu(),producte.getMagatzem(),producte.getEstanteria());
                }
            }catch (EOFException e){
                System.out.println(Color.RED + Color.ITALIC + "ERROR"+ Color.RESET);
            }catch (StreamCorruptedException x){}
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error el fitxer Productes no existeix");
    }
    //Ex2
    //CrearFitxer

    /**
     * Crear un fitxer aleatori
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void Ex2_Create() throws IOException, ClassNotFoundException{
        File file = new File("./Productes.dat");
        if (file.exists()){
            ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(file));
            Producte producte;

            List<Integer> codis = new ArrayList<>();
            List<String> noms = new ArrayList<>();
            List<Double> preus = new ArrayList<>();
            List<String> magatzems = new ArrayList<>();
            List<Integer> estanteries = new ArrayList<>();

            try {
                while (true){
                    producte = (Producte) dataIS.readObject();
                    codis.add(producte.getCodi());
                    noms.add(producte.getNom());
                    preus.add(producte.getPreu());
                    magatzems.add(producte.getMagatzem());
                    estanteries.add(producte.getEstanteria());
                }
            }catch (EOFException e){
                System.out.print("");
            }catch (StreamCorruptedException x){}

            File fileAl = new File("./AleatoriProductes.dat");
            RandomAccessFile ramdomfile = new RandomAccessFile(fileAl, "rw");
            StringBuffer buffer = null;

            for (int j = 0; j < codis.size(); j++) {
                ramdomfile.writeInt(codis.get(j));
                buffer = new StringBuffer(noms.get(j));
                buffer.setLength(12);
                ramdomfile.writeChars(buffer.toString());
                ramdomfile.writeDouble(preus.get(j));
                buffer = new StringBuffer(magatzems.get(j));
                buffer.setLength(12);
                ramdomfile.writeChars(buffer.toString());
                ramdomfile.writeInt(estanteries.get(j));
            }
            System.out.println("S'ha introduit les dades al fitxer Productes correctament");
            ramdomfile.close();
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error el fitxer Productes no existeix");
    }

    //Legir fitxer

    /**
     * Llegir un fitxer aleatori
     * @throws IOException
     */
    public void Ex2_Read() throws IOException{
        File file = new File("./AleatoriProductes.dat");
        if (file.exists()) {
            RandomAccessFile randomFile = new RandomAccessFile(file, "r");

            int codi, estanteria, position = 0;
            double preu;
            char nom[] = new char[12], aux;
            char magatzem[] = new char[12];


            for (;;) {
                randomFile.seek(position);
                codi = randomFile.readInt();//4
                for (int i = 0; i <nom.length; i++) {
                    aux = randomFile.readChar();
                    nom[i] = aux;
                }
                String noms = new String(nom);//24
                preu = randomFile.readDouble();//8
                for (int i = 0; i <magatzem.length; i++) {
                    aux = randomFile.readChar();
                    magatzem[i] = aux;
                }
                String magatzemns = new String(magatzem);//24
                estanteria = randomFile.readInt();//4

                System.out.printf(Color.CYAN+ "Taula Productes");
                System.out.println("=========================" + Color.RESET);
                System.out.printf("Codi: %d, Nom: %s, Preu: %.1f, Magatzem: %s, Estanteria: %d \n",
                        codi,noms,preu,magatzemns,estanteria);

                position = position+64;
                if (randomFile.getFilePointer() == randomFile.length())
                    break;
            }
            randomFile.close();
        }else
            System.out.printf("No existeix el fitxer AleatoriProduct.dat");
    }

    //Ex3

    /**
     * Modificar salari
     * @throws IOException
     */
    public void Ex3() throws IOException{
        System.out.print(Color.ITALIC + "Quin codi vols modificar: ");
        scanner.nextLine();
        int id_c = scanner.nextInt();

        File file = new File("./AleatoriProductes.dat");
        if (file.exists()){
            RandomAccessFile randomFile = new RandomAccessFile(file, "rw");
            long posicio = (id_c - 1) * 64;
            randomFile.seek(posicio);
            int id = randomFile.readInt();
            System.out.println(id);
            if (id != id_c){
                System.out.println("No existeix aqiest codi");
            }else {
                posicio = posicio + 4 + 24;
                randomFile.seek(posicio);
                double salari_old = randomFile.readDouble();
                double salario_new = (((salari_old / 100) * 20) + salari_old);
                randomFile.seek(posicio);
                randomFile.writeDouble(salario_new);
                System.out.printf("El salari vell es %.1f i s'ha cambiar a %.1f \n",salari_old, salario_new);
            }
            System.out.println("S'ha modificat de manera correcta");
            randomFile.close();
        }else
            System.out.printf("No existeix el fitxer AleatoriProduct.dat");
    }

    //Ex4

    /**
     * Eliminar un producte
     * @throws IOException
     */
    public void Ex4() throws IOException, ClassNotFoundException{
        System.out.print(Color.ITALIC + "Quin codi vols eliminar: ");
        scanner.nextLine();
        int id_c = scanner.nextInt();

        File file = new File("./AleatoriProductes.dat");
        Path fileTxt = Path.of("./ProductesEliminats.txt");
        if (file.exists()) {
            RandomAccessFile randomFile = new RandomAccessFile(file, "rw");
            OutputStream outputStream = Files.newOutputStream(fileTxt, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            PrintStream printStream = new PrintStream(outputStream,true);

            int codi, estanteria, position = 0;
            double preu;
            char nom[] = new char[12], aux;
            char magatzem[] = new char[12];

            List<Integer> codis = new ArrayList<>();
            List<String> nomsl = new ArrayList<>();
            List<Double> preus = new ArrayList<>();
            List<String> magatzems = new ArrayList<>();
            List<Integer> estanteries = new ArrayList<>();

            for (;;) {
                randomFile.seek(position);
                codi = randomFile.readInt();//4
                for (int i = 0; i <nom.length; i++) {
                    aux = randomFile.readChar();
                    nom[i] = aux;
                }
                String noms = new String(nom);//24
                preu = randomFile.readDouble();//8
                for (int i = 0; i <magatzem.length; i++) {
                    aux = randomFile.readChar();
                    magatzem[i] = aux;
                }
                String magatzemns = new String(magatzem);//24
                estanteria = randomFile.readInt();//4
                if (codi == id_c){
                    printStream.println("Id: " + codi + "Nom: " + noms + "Preu: " + preu + "Magatzem: " + magatzemns + "Estanteria: " +estanteria);
                    position = position+64;
                    continue;
                }
                codis.add(codi);
                nomsl.add(noms);
                preus.add(preu);
                magatzems.add(magatzemns);
                estanteries.add(estanteria);
                position = position+64;
                if (randomFile.getFilePointer() == randomFile.length())
                    break;
            }
            StringBuffer buffer = null;

            for (int j = 0; j < codis.size(); j++) {
                randomFile.writeInt(codis.get(j));
                buffer = new StringBuffer(nomsl.get(j));
                buffer.setLength(12);
                randomFile.writeChars(buffer.toString());
                randomFile.writeDouble(preus.get(j));
                buffer = new StringBuffer(magatzems.get(j));
                buffer.setLength(12);
                randomFile.writeChars(buffer.toString());
                randomFile.writeInt(estanteries.get(j));
            }
            randomFile.close();

        }else
            System.out.printf("No existeix el fitxer AleatoriProduct.dat");
    }

    //Ex5

    /**
     * Ficar tots el fitxers de un directori a FitxersDelDirectori.txt
     * @throws IOException
     */
    public void Ex5() throws IOException{
        System.out.print("Introdueix el nom del directori que vols utilitzar: ");
        scanner.nextLine();
        String nom = scanner.nextLine();
        File dir = new File(nom);
        File[] files = dir.listFiles();
        Path file = Path.of("src/Fitxeros/FitxersDelDirectori.txt");
        OutputStream outputStream = Files.newOutputStream(file, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        PrintStream printStream = new PrintStream(outputStream,true);
        if (files == null ||files.length == 0){
            System.out.println("No hi ha elements en aquesta carpeta o la carpeta no existeix");
            return;
        }else {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile()){
                    printStream.println(files[i]);
                }
            }
        }
    }

    //Ex6

    /**
     * Eliminar fitxeros
     * @throws IOException
     */
    public void Ex6() throws IOException{
        Path file = Path.of("src/Fitxeros/FitxersDelDirectori.txt");
        OutputStream outputStream = Files.newOutputStream(file, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        PrintStream printStream = new PrintStream(outputStream,true);
        Scanner scannerFile = new Scanner(file);
        List<String> files = new ArrayList<>();
        while (scannerFile.hasNext()){
            String path = scannerFile.nextLine();
            files.add(path);
            System.out.println(path);
        }
        for (int i = 0; i < files.size(); i++) {
            File fileDel = new File(files.get(i));
            fileDel.delete();
        }
        System.out.println("Fitxers eliminats");
    }

    //Ex7

    /**
     * Añadir 5 fitxeros mas
     * @throws IOException
     */
    public void Ex7() throws IOException{
        Path file = Path.of("src/Fitxeros/FitxersDelDirectori.txt");
        OutputStream outputStream = Files.newOutputStream(file, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        PrintStream printStream = new PrintStream(outputStream,true);
        String files[] = {"OLa.txt","aaaaa.txt","bbb.dat","aaa.txt","ISrael.cvs"};

        for (int i = 0; i < files.length; i++) {
            printStream.println(files[i]);
        }
    }

    //Ex8
    public void Ex8() throws IOException{
        Path filecvs = Path.of("./restaurants.csv");
        Path file = Path.of("./restaurant08138.txt");
        OutputStream outputStream = Files.newOutputStream(file, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        PrintStream printStream = new PrintStream(outputStream,true);
        Scanner scannerFile = new Scanner(filecvs);
        Pattern pattern = Pattern.compile("08130");
        int total = 0;
        int findPostal = 0;
        while (scannerFile.hasNext()){
            String info = scannerFile.nextLine();
            Matcher matcher = pattern.matcher(info);
            if (matcher.find()) {
                findPostal++;
                printStream.println(info);
            }
            total++;
        }
        System.out.println("Total lineas " + total);
        System.out.println("Total lineas amb 08130 " + findPostal);
    }
}
