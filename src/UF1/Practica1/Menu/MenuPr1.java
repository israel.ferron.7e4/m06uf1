package UF1.Practica1.Menu;

import UF1.Practica1.Metodes.MetodesPr1;

import java.io.IOException;
import java.util.Scanner;

public class MenuPr1 extends Menu {
    MetodesPr1 metodesPr1;

    public MenuPr1(Scanner scanner){
        super(scanner);
        metodesPr1 = new MetodesPr1(scanner);
        addOption("Practica 1");
        addOption("Crear 5 Productes i ficar-los en el fitxer Productes.dat");
        addOption("Llegir el fitxer Productes.dat");
        addOption("Llegir el fitxer Productes.dat i crear un AleatoriProductes.dat");
        addOption("Llegir el fitxer AleatoriProductes.dat");
        addOption("Modificar el salari del fitxer AleatoriProductes.dat");
        addOption("Eliminar Producte");
        addOption("Posar el elfitxers de un directori en un fitxer .txt");
        addOption("Eliminar el fitxers que hi ha dintre del fitxer .txt");
        addOption("Afegir 5 noms més de fitxers al fitxer \"FitxersDelDirectori.txt\", no es poden perdre els\n" +
                "noms dels fitxers que ja tenia emmagatzemats.");
        addOption("Que llegeixi el fitxer \"restaurants.csv\" que trobaràs al Classroom i visualitzi quans\n" +
                "restaurants hi ha en total en el fitxer, quans restaurants hi ha amb el codi postal 08013, i que guardi en un altre\n" +
                "fitxer anomenat \"resturants08013.txt\" el nom dels restaurants situats en aquest codi postal.");
    }

    @Override
    public void action(int select) throws IOException, ClassNotFoundException {
        switch (select){
            case 1:
                metodesPr1.Ex1_Create();
                break;
            case 2:
                metodesPr1.Ex1_Read();
                break;
            case 3:
                metodesPr1.Ex2_Create();
                break;
            case 4:
                metodesPr1.Ex2_Read();
                break;
            case 5:
                metodesPr1.Ex3();
                break;
            case 6:
                metodesPr1.Ex4();
                break;
            case 7:
                metodesPr1.Ex5();
                break;
            case 8:
                metodesPr1.Ex6();
                break;
            case 9:
                metodesPr1.Ex7();
                break;
            case 10:
                metodesPr1.Ex8();
                break;
        }
    }

}