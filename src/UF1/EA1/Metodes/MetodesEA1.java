package UF1.EA1.Metodes;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetodesEA1 {
    Scanner scanner;

    public MetodesEA1(Scanner scanner) {
        this.scanner = scanner;
    }

    //Ex1
    public void Ex1(){
        System.out.print("Introdueix el nom del directori que vols utilitzar: ");
        scanner.nextLine();
        String nom = scanner.nextLine();
        File dir = new File(nom);
        File[] files = dir.listFiles();
        if (files == null ||files.length == 0){
            System.out.println("No hi ha elements en aquesta carpeta o la carpeta no existeix");
            return;
        }else {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile()){
                    System.out.println(files[i]);
                }
            }
        }
    }

    //Ex2
    public void Ex2(){
        System.out.print("Quin text li vols possar al fitxer nou?: ");
        scanner.nextLine();
        String frase = scanner.nextLine();
        String homePath = System.getProperty("user.home");
        Path pathFitxer =  Path.of("./");
        Path fitxer = pathFitxer.resolve("Fitxer");

        try {
            if (!Files.exists(fitxer))
                    Files.createFile(fitxer);
            OutputStream outputStream = Files.newOutputStream(fitxer, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            PrintStream printStream = new PrintStream(outputStream,true);
            printStream.println(frase);
            System.out.println("S'ha escrit al fitxer correctament");
        }catch (IOException e){
            System.err.println("Error!");
        }
    }

    //Ex3
    public void Ex3(){
        System.out.print("Quin text li vols possar al fitxer?: ");
        scanner.nextLine();
        String frase = scanner.nextLine();
        System.out.print("A quin fitxer vols posar aquesta frase?: ");
        String fitxer = scanner.nextLine();
        Path fitxerPath = Path.of(fitxer);

        try {
            if (!Files.exists(fitxerPath))
                System.out.println("El fitxer no existeix");
            else {
                OutputStream outputStream = Files.newOutputStream(fitxerPath, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                PrintStream printStream = new PrintStream(outputStream, true);
                printStream.println(frase);
                System.out.println("S'ha escrit al fitxer correctament");
            }
        }catch (IOException e){
            System.err.println("Error!");
        }
    }

    //Ex4
    public void Ex4(){
        System.out.print("Quin fitxer vols llegir?: ");
        scanner.nextLine();
        String fitxer = scanner.nextLine();
        Path path = Path.of(fitxer);

        try {
            Scanner filescaner = new Scanner(path);
            while (filescaner.hasNextLine()){
                System.out.println(filescaner.nextLine());
            }
        }catch (IOException e){
            System.err.println("Error!");
        }
    }

    //Ex6
    public void Ex6(){
        Path path = Path.of("./fitxers/enters.txt");
        int s = 0;
        int q = 0;

        try {
            Scanner fileScanner = new Scanner(path);
            while (fileScanner.hasNext()){
                q++;
                int n = fileScanner.nextInt();
                s += n;
            }
            System.out.println("Hi ha: " + q + " numeros.");
            System.out.println("La suma de tots els numeros es: " + s);
        }catch (IOException e){
            System.err.println("Error!");
        }
    }

    //Ex7
    public void Ex7(){
        System.out.print("Quin fitxer vols llegir?: ");
        scanner.nextLine();
        String fitxer = scanner.nextLine();
        Path path = Path.of(fitxer);
        String cadena,mayor = null, menor = null;

        try {
            Scanner scannerFile = new Scanner(path);
            if (scannerFile.hasNext()) {
                cadena = scannerFile.nextLine();
                mayor = menor = cadena;
            }
            while (scannerFile.hasNext()) {
                cadena = scannerFile.nextLine();
                if (cadena.length() > mayor.length())
                    mayor = cadena;
                else if (cadena.length() < menor.length())
                    menor = cadena;
            }
        }catch (IOException e){
            System.err.println("Error!");
        }

        System.out.println("La linea mes llarga es:");
        System.out.println(mayor);
        System.out.println("La linea mes curta es:");
        System.out.println(menor);
    }

    //Ex8
    public void Ex8(){
        System.out.print("Quin fitxer vols eliminar?: ");
        scanner.nextLine();
        String fitxer = scanner.nextLine();
        File file = new File(fitxer);

        if (file.delete()){
            System.out.println("S'ha eliminat el fitxer correctament");
        }else
            System.out.println("Error al eliminar fitxer");
    }

    //Ex9
    public void Ex9(){
        System.out.print("Quin fitxer vols llegir?: ");
        scanner.nextLine();
        String fitxer = scanner.nextLine();
        File file = new File(fitxer);
        boolean existFile, isDirectory;

        if (file.exists()){
            existFile = true;
        }else
            existFile = false;

        if (file.isDirectory())
            isDirectory = true;
        else
            isDirectory = false;

        System.out.println("Existeix el path?: " + existFile);
        System.out.println("Es un directori?: " + isDirectory);
        if (!isDirectory){
            System.out.println("El nom del fitxer es: " + file.getName());
            System.out.println("La mida del fitxer es: " + file.getTotalSpace());
            System.out.println("Lectira: " + file.canRead() + "Escritura" + file.canWrite() + "Execucio" + file.canExecute());
        }
    }

    //Ex10
    public void Ex10(){
        Path path = Path.of("fitxers/restaurants.csv");
        ArrayList<String> restaurants = new ArrayList<>();
        Pattern pattern = Pattern.compile("Eixample");
        try {
            Scanner scannerFile = new Scanner(path);
            while (scannerFile.hasNext()){
                String info = scannerFile.nextLine();
                Matcher matcher = pattern.matcher(info);
                if (matcher.find())
                    restaurants.add(info);
            }
            for (String i: restaurants) {
                System.out.println(i);
            }
        }catch (IOException e){
        System.err.println("Error!");
        }
    }

    //Ex11
    public void Ex11() {
        Path path = Path.of("fitxers/restaurants.csv");
        System.out.print("Quin es el nom del bar?: ");
        scanner.nextLine();
        String nom = scanner.nextLine();
        System.out.print("Quin es el telefon del bar?: ");
        String tel = scanner.nextLine();
        System.out.print("Quin es la direccion del bar?: ");
        String dir = scanner.nextLine();
        System.out.print("Quin es el districte del bar?: ");
        String dis = scanner.nextLine();
        System.out.print("Quin es el barri del bar?: ");
        String barri = scanner.nextLine();
        System.out.print("Quin es la ciutat del bar?: ");
        String ciu = scanner.nextLine();
        System.out.print("Quin es el CP del bar?: ");
        String cp = scanner.nextLine();
        System.out.print("Quin es el pais del bar?: ");
        String pais = scanner.nextLine();
        System.out.print("Quin es la latitud del bar?: ");
        String lat = scanner.nextLine();
        System.out.print("Quin es la longitud del bar?: ");
        String lon = scanner.nextLine();
        System.out.print("Quin es la web del bar?: ");
        String web = scanner.nextLine();
        System.out.print("Quin es el mail del bar?: ");
        String mail = scanner.nextLine();

        try {
            if (!Files.exists(path))
                System.out.println("El fitxer no existeix");
            else {
                OutputStream outputStream = Files.newOutputStream(path, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                PrintStream printStream = new PrintStream(outputStream, true);
                printStream.println(nom +","+tel+","+dir+","+dis+","+barri+","+ciu+","+cp+","+pais+","+lon+","+lat+","+web+","+mail);
                System.out.println("S'ha escrit al fitxer correctament");
            }
        }catch (IOException e){
            System.err.println("Error!");
        }
    }

    //Ex12
    public void Ex12(){
        Path home = Path.of("fitxers/");
        Path file = home.resolve("restaurants.csv");
        Path copy = home.resolve("restarants2.csv");

        try {
            if (!Files.exists(copy)) {
                Files.copy(file, copy);
                System.out.println("S'ha copiat correctament");
            }else{
                Files.delete(copy);
                Files.copy(file, copy);
                System.out.println("S'ha copiat correctament");
            }

        }catch (IOException e){
            System.err.println("Error!");
        }
    }
}
