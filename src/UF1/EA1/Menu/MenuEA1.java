package UF1.EA1.Menu;

import UF1.EA1.Metodes.MetodesEA1;

import java.util.Scanner;

public class MenuEA1 extends Menu {
    MetodesEA1 metodesEA1;

    public MenuEA1(Scanner scanner){
        super(scanner);
        metodesEA1 = new MetodesEA1(scanner);
        addOption("Exercicis UF1.EA1 UF1");
        addOption("Llistar el nom de tots els fitxers d'un directori que li passem per teclat.");
        addOption("Omplir un fitxer creat de nou amb els textos que li passes per teclat.");
        addOption("Afegir el que escrius per teclat al final d'un fitxer de text ja existent.");
        addOption("Llegir línia a línia el contingut d'un fitxer de text i mostrar-lo per pantalla.");
        addOption("Llegir un fitxer línia a línia utilitzant la clase Scanner.");
        addOption("Llegir el fitxer enters.txt que trobaràs a la carpeta fitxers al Classroom i que mostri els números per\n" +
                "pantalla, digui quants números hi i ha i mostri també la suma de tots els números.");
        addOption("Obtenir la línia més llarga i la més curta d'un fitxer de text.");
        addOption("Eliminar el fitxer que s'introdueixi la ruta pel teclat.");
        addOption("Comprovacions bàsiques de una ruta");
        addOption("Que llegeixi el fitxer \"restaurants.csv\" que trobaràs a la carpeta \"fitxers\" al Classroom i mostri les dades\n" +
                "de tots els restaurants que es trobin a l'Eixample.");
        addOption("Que permeti afegir per teclat nous restaurants al fitxer \"restaurants.csv\" utilitzant el mateix format que els\n" +
                "que ja hi són (Nom,Telefon,Direccio,Districte,.....).");
        addOption("Que faci una còpia del fitxer \"restaurants.csv\" que es digui “restaurants2.csv” que contingui les dades de\n" +
                "tots els restaurants que no estiguin a l'Eixample.");
    }

    @Override
    public void action(int select){
        switch (select){
            case 1:
                metodesEA1.Ex1();
                break;
            case 2:
                metodesEA1.Ex2();
                break;
            case 3:
                metodesEA1.Ex3();
                break;
            case 4:
                metodesEA1.Ex4();
                break;
            case 6:
                metodesEA1.Ex6();
                break;
            case 7:
                metodesEA1.Ex7();
                break;
            case 8:
                metodesEA1.Ex8();
                break;
            case 9:
                metodesEA1.Ex9();
                break;
            case 10:
                metodesEA1.Ex10();
                break;
            case 11:
                metodesEA1.Ex11();
                break;
            case 12:
                metodesEA1.Ex12();
                break;
        }
    }

}
