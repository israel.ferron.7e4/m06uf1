package UF1.EA1;

import UF1.EA1.Menu.Color;
import UF1.EA1.Menu.MenuEA1;

import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuEA1 menuEA1 = new MenuEA1(scanner);
        menuEA1.showMenu();

        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!" + Color.RESET);
    }
}
