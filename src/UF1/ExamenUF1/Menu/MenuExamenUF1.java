package UF1.ExamenUF1.Menu;

import UF1.ExamenUF1.Metodes.MetodesExamenUF1;

import java.io.IOException;
import java.util.Scanner;

public class MenuExamenUF1 extends Menu {
    MetodesExamenUF1 metodesExamenUF1;

    public MenuExamenUF1(Scanner scanner) {
        super(scanner);
        metodesExamenUF1 = new MetodesExamenUF1(scanner);
        addOption("Examen UF1");
        addOption("Llegir el fitxer JSON \"restaurants.json\" que trobaràs al Classroom convertint cada fila del\n" +
                "fitxer a un objecte i mostra les dades dels objectes per pantalla.");
        addOption("Escriure un fitxer JSON nou anomenat \"restaurantsMeus.json\" que guardi les dades de tres\n" +
                "restaurants nous i les seves valoracions per data. Crea els objectes que necessitis per programa.");
        addOption("Llegir el fitxer JSON \"restaurants.json\" i calcula quantes valoracions (grades) ha tingut el\n" +
                "restaurant amb \"restaurant_id\": \"30191841\", i suma tots els \"scores\" del restaurant. Mostra els resultats per\n" +
                "pantalla.");
        addOption("Guardar els objectes obtinguts al llegir el fitxer \"restaurants.json\" en un fitxer binari\n" +
                "anomenat \"restaurants.dat\".");
        addOption("Leer fixxher Restaurants.dat");
        addOption("Crea un fitxer XML anomenat \"restaurants.xml\" amb els objectes emmagatzemats en el\n" +
                "fitxer \"restaurants.dat\". Si el teu fitxer \"restaurants.dat\" no es pot llegir correctament, crea una llista d'objectes\n" +
                "Restaurant per programa que reflecteixin les dades dels restaurants i crea el teu propi fitxer\n" +
                "\"restaurantsMeus.xml\".");
        addOption("Llegeix el fitxer \"restaurants.xml\" o el que hagis creat tu el \"restaurantsMeus.xml\" i mostra\n" +
                "les dades per pantalla.");
        addOption("Torna a llegir el fitxer \"restaurants.xml\" o \"restaurantsMeus.xml\" i utilitza els objectes\n" +
                "que obtens al llegir el fitxer per modificar el codi postal del restaurant amb restaurant_id\": \"30075445\", el nou\n" +
                "\"zipcode\" ara serà : \"10470\". Modifica també el \"mark\" de la data \"1988-05-01\" del restaurant amb\n" +
                "\"restaurant_id\": \"30191841\". El nou \"mark\" serà \"C\" Crea un nou fitxer XML anomenat \"restaurantsMod.xml\"\n" +
                "que tingui les dades modificades.");
    }

    @Override
    public void action(int select) throws IOException {
        switch (select) {
            case 1:
                metodesExamenUF1.Ex1();
                break;
            case 2:
                metodesExamenUF1.Ex2();
                break;
            case 3:
                metodesExamenUF1.Ex3();
                break;
            case 4:
                metodesExamenUF1.Ex4();
                break;
            case 5:
                metodesExamenUF1.Ex5();
                break;
            case 6:
                metodesExamenUF1.Ex6();
                break;
            case 7:
                metodesExamenUF1.Ex7();
                break;
            case 8:
                metodesExamenUF1.Ex8();
                break;
        }
    }
}