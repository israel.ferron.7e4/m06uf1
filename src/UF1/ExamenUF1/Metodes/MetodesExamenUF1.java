package UF1.ExamenUF1.Metodes;

import UF1.EA4.Menu.Color;
import UF1.ExamenUF1.Classes.ListaRestaurante;
import UF1.ExamenUF1.Classes.Address;
import UF1.ExamenUF1.Classes.Grades;
import UF1.ExamenUF1.Classes.Restaurant;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import java.io.*;
import java.util.*;

public class MetodesExamenUF1 {
    Scanner scanner;

    public MetodesExamenUF1(Scanner scanner) {
        this.scanner = scanner;
    }

    /**
     * Leer el fitxero restaurants.json
     */
    public void Ex1() {
        Gson gson = new Gson();
        String file = "";
        try (BufferedReader br = new BufferedReader(new FileReader("restaurants.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                file += linea;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Restaurant[] restaurants = gson.fromJson(file, Restaurant[].class);
        for (Restaurant r : restaurants){
            System.out.println(r);
        }
    }

    /**
     * Crear tres restaurantes en misRestaurantes.json
     */
    public void Ex2(){
        Gson gson = new Gson();
        List<Restaurant> restaurants = new ArrayList<>();
        String file = "";


        restaurants.add(new Restaurant(new Address(1,new ArrayList<Double>(Collections.singleton(2.5)),"SAAAA",24123),"AAAAAA","BBBB", new ArrayList<Grades>(Collections.singleton(new Grades("12-2-2142", 'G', 56))),"Restaurant1","11111"));
        restaurants.add(new Restaurant(new Address(1,new ArrayList<Double>(Collections.singleton(2.5)),"AADAS",243),"BBBBB","CCCC", new ArrayList<Grades>(Collections.singleton(new Grades("12-2-2142", 'A', 26))),"Restaurant2","22222"));
        restaurants.add(new Restaurant(new Address(1,new ArrayList<Double>(Collections.singleton(2.5)),"gasdasd",241),"CCCCC","DDDD", new ArrayList<Grades>(Collections.singleton(new Grades("12-2-2142", 'F', 6))),"Restaurant3","33333"));

        String json = gson.toJson(restaurants);

        System.out.println(json);

        try (BufferedWriter bw = new BufferedWriter(new FileWriter("misRestaurants.json"))) {
            bw.write(json);
            System.out.println("Fichero creado");
        } catch (IOException ex) {
        }
    }

    /**
     * Buscar la id de un restaurante i sumar su puntuacion i contar las notas
     */
    public void Ex3(){
        Gson gson = new Gson();
        String file = "";
        int countGrades = 0;
        int total = 0;
        try (BufferedReader br = new BufferedReader(new FileReader("restaurants.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                file += linea;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Restaurant[] restaurants = gson.fromJson(file, Restaurant[].class);
        for (Restaurant r : restaurants){
            if (r.getRestaurant_id().equals("30191841")){
                for (int i = 0; i < r.getGrades().size(); i++) {
                    total += r.getGrades().get(i).getScore();
                    countGrades++;
                }
                System.out.println(r);
                System.out.println(total);
                System.out.println(countGrades);
            }
        }
    }

    /**
     * Leer el fitxero restaurants.json i crear un fitxeri binario con esas dadas
     * @throws IOException
     */
    public void Ex4() throws IOException {
        Gson gson = new Gson();
        String file = "";
        try (BufferedReader br = new BufferedReader(new FileReader("restaurants.json"))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                file += linea;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Restaurant[] restaurants = gson.fromJson(file, Restaurant[].class);

        File fitch = new File("./Restaurants.dat");
        FileOutputStream fileOut = new FileOutputStream(fitch, true);
        ObjectOutputStream dataOS = new ObjectOutputStream(fileOut);
        for (Restaurant r : restaurants){
            dataOS.writeObject(r);
            System.out.println("Dada posada correctament");
        }
    }

    /**
     * Leer el fitxero restaurants.dat
     * @throws IOException
     */
    public void Ex5() throws IOException {
        File file = new File("./Restaurants.dat");
        if (file.exists()) {
            ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(file));
            Restaurant restaurant;

            int i = 1;
            System.out.printf(Color.CYAN+ "Taula Restaurants");
            System.out.println("=========================" + Color.RESET);
            try {
                while (true){
                    restaurant = (Restaurant) dataIS.readObject();
                    System.out.println(restaurant);
                }
            }catch (EOFException e){
                System.out.println(Color.RED + Color.ITALIC + "ERROR"+ Color.RESET);
            }catch (StreamCorruptedException x){} catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error el fitxer Departaments no existeix");
    }

    /**
     * Crear un fichero Restaurants.xml apartit de un Rstaurant.dat
     * @throws IOException
     */
    public void Ex6() throws IOException {
        File file = new File("./Restaurants.dat");
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(file));
        ListaRestaurante listres = new ListaRestaurante();
        if (file.exists()) {
            Restaurant restaurant;

            int i = 1;
            System.out.printf(Color.CYAN+ "Taula Restaurants");
            System.out.println("=========================" + Color.RESET);
            try {
                while (true){
                    restaurant = (Restaurant) dataIS.readObject();
                    listres.add(restaurant);
                }
            }catch (EOFException e){
                System.out.println(Color.RED + Color.ITALIC + "ERROR"+ Color.RESET);
            }catch (StreamCorruptedException x){} catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error el fitxer Departaments no existeix");
        dataIS.close();
        try {
            XStream xstream = new XStream();
            xstream.alias("ListaRestaurants",
                    ListaRestaurante.class);
            xstream.alias("Restaurant", Restaurant.class);
            xstream.alias("Grades", Grades.class);
            xstream.addImplicitCollection
                    (ListaRestaurante.class, "ListaRestaurants");
            xstream.toXML(listres, new
                    FileOutputStream("Restaurants.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e)
        {e.printStackTrace();}
    }

    /**
     * Leer el fichero Restaurant.xml
     * @throws FileNotFoundException
     */
    public void Ex7() throws FileNotFoundException {
        XStream xstream = new XStream();

        xstream.alias("ListaRestaurants", ListaRestaurante.class);
        xstream.alias("Restaurant", Restaurant.class);
        xstream.alias("Grades", Grades.class);
        xstream.addImplicitCollection(ListaRestaurante.class, "ListaRestaurants");

        ListaRestaurante listadoTodas = (ListaRestaurante)
                xstream.fromXML(new FileInputStream("Restaurants.xml"));
        System.out.println("Numero de Personas: " +
                listadoTodas.getListaPersonas().size());

        List<Restaurant> listaRestaurantes = new ArrayList<Restaurant>();
        listaRestaurantes = listadoTodas.getListaPersonas();

        Iterator iterador = listaRestaurantes.listIterator();
        while( iterador.hasNext() ) {
            Restaurant p = (Restaurant) iterador.next();
            System.out.println(p);
        }
        System.out.println("Fin de listado .....");
    }

    /**
     * Modificar el fitxero Restaurants.xml
     * @throws FileNotFoundException
     */
    public void Ex8() throws FileNotFoundException {
        XStream xstream = new XStream();
        List<Restaurant> restautantsNew = new ArrayList<>();

        xstream.alias("ListaRestaurants", ListaRestaurante.class);
        xstream.alias("Restaurant", Restaurant.class);
        xstream.alias("Grades", Grades.class);
        xstream.addImplicitCollection(ListaRestaurante.class, "ListaRestaurants");

        ListaRestaurante listadoTodas = (ListaRestaurante)
                xstream.fromXML(new FileInputStream("Restaurants.xml"));
        System.out.println("Numero de Personas: " +
                listadoTodas.getListaPersonas().size());

        List<Restaurant> listaRestaurantes = new ArrayList<Restaurant>();
        listaRestaurantes = listadoTodas.getListaPersonas();

        Iterator iterador = listaRestaurantes.listIterator();
        while( iterador.hasNext() ) {
            Restaurant p = (Restaurant) iterador.next();
            if (p.getRestaurant_id().equals("30075445")) {
                Address a = p.getAddress();
                List<Double> cord = a.getCoords();
                p.setAddress(new Address(a.getBuilding(), (ArrayList<Double>) cord, a.getStreet(), 10470));
                restautantsNew.add(p);
            }else if (p.getRestaurant_id().equals("30191841")){
                for (int i = 0; i < p.getGrades().size(); i++) {
                    if (p.getGrades().get(i).getDate().equals("1988-05-01")){
                        p.getGrades().get(i).setMark('C');
                    }
                }
                restautantsNew.add(p);
            }else
                restautantsNew.add(p);
        }

        try {
            XStream xstreamnew = new XStream();
            xstreamnew.alias("ListaRestaurants",
                    ListaRestaurante.class);
            xstreamnew.alias("Restaurant", Restaurant.class);
            xstreamnew.alias("Grades", Grades.class);
            xstreamnew.addImplicitCollection
                    (ListaRestaurante.class, "ListaRestaurants");
            xstreamnew.toXML(restautantsNew, new
                    FileOutputStream("Restaurantsmod.xml"));
            System.out.println("Creado fichero XML....");

        }catch (Exception e)
        {e.printStackTrace();}
    }
}