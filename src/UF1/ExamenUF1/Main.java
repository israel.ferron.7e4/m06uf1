package UF1.ExamenUF1;

import UF1.EA4.Menu.Color;
import UF1.ExamenUF1.Menu.MenuExamenUF1;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        MenuExamenUF1 menuExamenUF1 = new MenuExamenUF1(scanner);
        menuExamenUF1.showMenu();

        System.out.println(Color.CYAN + Color.BOLD + Color.ITALIC + "Gracies per la teva visita!!" + Color.RESET);
        }
    }
