package UF1.ExamenUF1.Classes;

import java.util.ArrayList;
import java.util.List;

public class ListaRestaurante {
    private List<Restaurant> ListaRestaurants = new ArrayList<>();

    public ListaRestaurante() {
    }

    public void add(Restaurant res){
        ListaRestaurants.add(res);
    }

    public List<Restaurant> getList() {
        return ListaRestaurants;
    }

    public void setList(List<Restaurant> list) {
        this.ListaRestaurants = list;
    }

    public List<Restaurant> getListaPersonas() {
        return ListaRestaurants;
    }
}
