package UF1.ExamenUF1.Classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Address implements Serializable
{
    private int building;
    private ArrayList<Double> coords;
    private String street;
    private int zipcode;

    public Address(int building, ArrayList<Double> coords, String street, int zipcode) {
        this.building = building;
        this.coords = coords;
        this.street = street;
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "building=" + building +
                ", coords=" + coords +
                ", street='" + street + '\'' +
                ", zipcode=" + zipcode +
                '}';
    }

    public int getBuilding() {
        return building;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public List<Double> getCoords() {
        return coords;
    }

    public void setCoords(ArrayList<Double> coords) {
        this.coords = coords;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }
}
