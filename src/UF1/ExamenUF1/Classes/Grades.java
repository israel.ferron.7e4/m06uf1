package UF1.ExamenUF1.Classes;

import java.io.Serializable;

public class Grades implements Serializable {
    private String date;
    private char mark;
    private int score;

    public Grades(String date, char mark, int score) {
        this.date = date;
        this.mark = mark;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Grades{" +
                "date=" + date +
                ", mark=" + mark +
                ", score=" + score +
                '}';
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public char getMark() {
        return mark;
    }

    public void setMark(char mark) {
        this.mark = mark;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
