package UF1.EA4.Prova;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class EscribirFitchObject {
    public static void main(String[] args) throws IOException {
        Persona persona;

        File file = new File("./src/Prova/Fitchero.dat");
        FileOutputStream fileOut = new FileOutputStream(file,true);
        ObjectOutputStream dataOS = new ObjectOutputStream(fileOut);

        String nombres[] = {"Anna", "Luis","Alicia", "Israel"};
        int edats[] = {21,67,34,20};
        System.out.println("GRABO LOS DATOS DE  LAS PERSONAS");
        for (int i = 0; i < edats.length; i++) {
            persona = new Persona(nombres[i],edats[i]);
            dataOS.writeObject(persona);
            System.out.println("GRABO LOS DATOS DE " + nombres[i]);
        }
        dataOS.close();
    }
}
