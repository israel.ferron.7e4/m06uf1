package UF1.EA4.Prova;

import java.io.*;

public class LeerFitchObjeto {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Persona persona;
        File file = new File("./src/Prova/Fitchero.dat");
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(file));

        int i = 1;
        try {
            while (true){
                persona = (Persona)dataIS.readObject();
                System.out.print(i + "=>");
                i++;
                System.out.printf("Nombre %s, edad: %d %n",
                        persona.getNombre(),persona.getEdad());
            }
        }catch (EOFException e){
            System.out.println("ERROR");
        }catch (StreamCorruptedException x){}

        dataIS.close();
    }
}
