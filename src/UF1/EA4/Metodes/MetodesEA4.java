package UF1.EA4.Metodes;

import UF1.EA4.Classes.Departament;
import UF1.EA4.Classes.Empleat;
import UF1.EA4.ClassesGuia.MiObjectOutputStream;
import UF1.EA4.Menu.Color;


import java.io.*;
import java.util.Scanner;

public class MetodesEA4 {
    Scanner scanner;

    //Contructor
    public MetodesEA4(Scanner scanner) {
        this.scanner = scanner;
    }

    /**
     *Crear els fitxers de Objectes 5 per departaments
     * @throws IOException
     */
    ;
    public void Ex1_Dep() throws IOException{
        File file = new File("./FitxerDepartaments.dat");
        if (!file.exists()) {
            Departament departament;
            FileOutputStream fileOut = new FileOutputStream(file, true);
            ObjectOutputStream dataOS = new ObjectOutputStream(fileOut);

            int ids[] = {1, 2, 3, 4, 5};
            String nomsdep[] = {"I+D", "Finances", "Contabilitat", "Marketing", "Informàtica"};
            String localitats[] = {"Bombay", "Medellin", "Murcia", "Camberra", "Tokyo"};

            for (int i = 0; i < ids.length; i++) {
                departament = new Departament(ids[i], nomsdep[i], localitats[i]);
                dataOS.writeObject(departament);
            }
            System.out.println("S'ha introduit les dades al fitxer Departaments correctament");
            dataOS.close();
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error algun dels fitxers Departaments ja existeix"+ Color.RESET);
    }

    /**
     * Crear els fitxers de Objectes 5 per empleats
     * @throws IOException
     */
    public void Ex1_Emp() throws IOException{
        File file = new File("./FitxerEmpleats.dat");
        if (!file.exists()) {
            Empleat empleat;
            FileOutputStream fileOut = new FileOutputStream(file, true);;
            ObjectOutputStream dataOS = new ObjectOutputStream(fileOut);

            int ids[] = {1, 2, 3, 4, 5};
            String cognoms[] = {"Ferron", "Julian", "Martinez", "Rebollo", "Fernandez"};
            int deps[] = {10, 20, 30, 40, 50};
            double salaris[] = {4521.5, 2162.2, 6123.6, 6125.7, 1252.6};

            for (int i = 0; i < ids.length; i++) {
                empleat = new Empleat(ids[i],cognoms[i],deps[i],salaris[i]);
                dataOS.writeObject(empleat);
            }
            System.out.println("S'ha introduit les dades al fitxer Empleats correctament");
            dataOS.close();
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error algun dels fitxers Empleats ja existeix" + Color.RESET);
    }

    /**
     * Llegit el fitxer departaments
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void Ex2_Dep() throws IOException, ClassNotFoundException{
        File file = new File("./FitxerDepartaments.dat");
        if (file.exists()) {
            ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(file));
            Departament departament;

            int i = 1;
            System.out.printf(Color.CYAN+ "Taula Departaments");
            System.out.println("=========================" + Color.RESET);
            try {
                while (true){
                    departament = (Departament) dataIS.readObject();
                    System.out.print(Color.PURPLE + i + "=>");
                    i++;
                    System.out.printf("ID: %d, Nom: %s, Localitat: %s %n",departament.getId(),departament.getNom(),departament.getLocalitat());
                }
            }catch (EOFException e){
                System.out.println(Color.RED + Color.ITALIC + "ERROR"+ Color.RESET);
            }catch (StreamCorruptedException x){}
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error el fitxer Departaments no existeix");
    }

    /**
     * Llegir el fitxer empleats
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void Ex2_Emp() throws IOException, ClassNotFoundException{
        File file = new File("./FitxerEmpleats.dat");
        if (file.exists()) {
            ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(file));
            Empleat empleat;

            int i = 1;
            System.out.printf(Color.CYAN + "Taula Empleats");
            System.out.println("=======================" + Color.RESET);
            try {
                while (true){
                    empleat = (Empleat) dataIS.readObject();
                    System.out.print(Color.PURPLE + i + "=>");
                    i++;
                    System.out.printf("ID: %d, Cognom: %s, Departamen: %d, Salari: %.1f/n",empleat.getId(),empleat.getCognom(),empleat.getDep(), empleat.getSalari());
                    System.out.println("-------------------------------");
                }
            }catch (EOFException e){
                System.out.println(Color.RED + Color.ITALIC + "ERROR"+ Color.RESET);
            }catch (StreamCorruptedException x){}
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error el fitxer Empleats no existeix");
    }

    public void Ex3_Dep() throws IOException{
        File file = new File("./FitxerDepartaments.dat");
        if (file.exists()) {
            Departament departament;
            MiObjectOutputStream dataOS = new MiObjectOutputStream(new FileOutputStream(file,true));

            int ids[] = {6, 7, 8, 9, 10};
            String nomsdep[] = {"I+D", "Finances", "Contabilitat", "Marketing", "Informàtica"};
            String localitats[] = {"Bombay", "Medellin", "Murcia", "Camberra", "Tokyo"};

            for (int i = 0; i < ids.length; i++) {
                departament = new Departament(ids[i], nomsdep[i], localitats[i]);
                dataOS.writeObject(departament);
            }
            System.out.println("S'ha introduit les dades al fitxer Departaments correctament");
            dataOS.close();
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error algun dels fitxers Departaments ja existeix"+ Color.RESET);
    }

    public void Ex3_Emp() throws IOException{
        File file = new File("./FitxerEmpleats.dat");
        if (file.exists()) {
            Empleat empleat;
            MiObjectOutputStream dataOS = new MiObjectOutputStream(new FileOutputStream(file,true));

            int ids[] = {6, 7, 8, 9, 10};
            String cognoms[] = {"Picazos", "Virgili", "Peces", "Colomer", "Brun"};
            int deps[] = {10, 60, 20, 40, 50};
            double salaris[] = {4821.5, 6562.2, 5623.6, 9125.7, 2252.6};

            for (int i = 0; i < ids.length; i++) {
                empleat = new Empleat(ids[i],cognoms[i],deps[i],salaris[i]);
                dataOS.writeObject(empleat);
            }
            System.out.println("S'ha introduit les dades al fitxer Departaments correctament");
            dataOS.close();
        }else
            System.out.println(Color.RED + Color.ITALIC + "Error algun dels fitxers Departaments ja existeix"+ Color.RESET);
    }
}
