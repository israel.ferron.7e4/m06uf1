package UF1.EA4.Classes;

import java.io.Serializable;

public class Empleat implements Serializable {
    private int id;
    private String cognom;
    private int dep;
    private double salari;

    public Empleat(int id, String cognom, int dep, double salari) {
        this.id = id;
        this.cognom = cognom;
        this.dep = dep;
        this.salari = salari;
    }

    public Empleat(){
        this.cognom=null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public int getDep() {
        return dep;
    }

    public void setDep(int dep) {
        this.dep = dep;
    }

    public double getSalari() {
        return salari;
    }

    public void setSalari(double salari) {
        this.salari = salari;
    }
}
