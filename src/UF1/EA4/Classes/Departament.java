package UF1.EA4.Classes;

import java.io.Serializable;

public class Departament implements Serializable {
    private int id;
    private String nom;
    private String localitat;

    public Departament(int id, String nom, String localitat) {
        this.id = id;
        this.nom = nom;
        this.localitat = localitat;
    }

    public Departament(){
        this.nom=null;
        this.localitat=null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLocalitat() {
        return localitat;
    }

    public void setLocalitat(String localitat) {
        this.localitat = localitat;
    }
}
