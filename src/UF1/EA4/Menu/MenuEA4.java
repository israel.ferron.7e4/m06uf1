package UF1.EA4.Menu;

import UF1.EA4.Metodes.MetodesEA4;

import java.io.IOException;
import java.util.Scanner;

public class MenuEA4 extends Menu {
    MetodesEA4 metodesEA4;

    public MenuEA4(Scanner scanner){
        super(scanner);
        metodesEA4 = new MetodesEA4(scanner);
        addOption("Exercicis UF1.EA4 U1");
        addOption("1-Crea els mètodes per escriure 5 objectes Empleat en un fitxer anomenat \"FitxerDepartaments\"");
        addOption("2-Crea els mètodes per escriure 5 objectes Departament en un fitxer anomenat \"FitxerEmpleats\"");
        addOption("3-Llegir els fitxers \"FitxerDepartamets\"");
        addOption("4-Llegir els fitxers \"FitxerEmpleat\"");
        addOption("5-Afegeix 5 objectes Departamets al fitxer \"FitxerDepartaments\"");
        addOption("6-Afegeix 5 objectes Empleats al fitxer \"FitxerEmpleats\"");
    }

    @Override
    public void action(int select) throws IOException, ClassNotFoundException {
        switch (select){
            case 1:
                metodesEA4.Ex1_Dep();
                break;
            case 2:
                metodesEA4.Ex1_Emp();
                break;
            case 3:
                metodesEA4.Ex2_Dep();
                break;
            case 4:
                metodesEA4.Ex2_Emp();
                break;
            case 5:
                metodesEA4.Ex3_Dep();
                break;
            case 6:
                metodesEA4.Ex3_Emp();
                break;

        }
    }

}