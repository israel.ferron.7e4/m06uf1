package UF1.JSON.Ej1;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class LeerJsonEj1 {
    public static void main(String[] args) {
        Gson gson = new Gson();
        String file = "";

        try (BufferedReader br = new BufferedReader(new FileReader("persona1.json"))){
            String linea;
            while ((linea = br.readLine()) != null){
                file += linea;
            }
        }catch (FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        Properties properties = gson.fromJson(file,Properties.class);
        System.out.println(properties.get("nombre"));
        System.out.println(properties.get("apellidos"));
        System.out.println(properties.get("edad"));
    }
}
